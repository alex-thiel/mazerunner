package de.alx_development.MazeRunner.Generator;

/*-
 * #%L
 * MazeRunner configuration management
 * %%
 * Copyright (C) 2019 ALX-Development
 * %%
 * This file is part of MazeRunner.
 *
 * MazeRunner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MazeRunner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil des MazeRunner Projekt.
 *
 * MazeRunner ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * MazeRunner wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * @author Alexander Thiel
 */
public class GeneratorFactory {

    /**
     * This function returns an instance of the generator as specified with
     * the generator description.
     *
     * @param description The generator description
     * @return Returns an instance of the generator
     */
    public static Generator getGenerator(GeneratorDescription description) {

        /*
         * Instantiating the generator using reflection
         */
        Class<?> clazz;
        try {
            clazz = Class.forName(description.getClassname());
            Constructor<?> constructor = clazz.getConstructor();
            Object instance = constructor.newInstance();
            Generator generator = (Generator) instance;

            /*
             * Cloning all properties
             */
            generator.setName(description.getName());
            for (String key : description.getProperties()) {
                generator.addProperty(key, description.getPropertyValue(key));
            }

            return generator;

        } catch (ClassNotFoundException | NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e1) {
            e1.printStackTrace();
        }

        return null;
    }

    /**
     * This function may be used to extract the description of a generator from
     * it's containing generator instance. It is used by configurations to
     * prepare the description for persistent storage.
     *
     * @param generator The generator which description is requested
     * @return The generators description object
     */
    public static GeneratorDescription getDescription(Generator generator) {
        return generator.getDescription();
    }
}
