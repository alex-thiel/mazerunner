package de.alx_development.MazeRunner.Generator;

/*-
 * #%L
 * MazeRunner configuration management
 * %%
 * Copyright (C) 2019 ALX-Development
 * %%
 * This file is part of MazeRunner.
 *
 * MazeRunner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MazeRunner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil des MazeRunner Projekt.
 *
 * MazeRunner ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * MazeRunner wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Alexander Thiel
 */
public class GeneratorDescription {

    /**
     * The properties are the settings information for each generator. Using
     * different properties in different generator instances enables the system
     * to do several actions with the same generator implementation.
     */
    private final Map<String, String> properties = new HashMap<>();
    /**
     * In the dependencies list all names of generators, which needs to be
     * executed before executing this generator are listed.
     */
    private final Set<String> dependencies = new HashSet<>();
    /**
     * Internal used name to identify the generator inside the configuration
     */
    private String name;
    /**
     * The classname to be loaded by the generator factory
     */
    private String classname;

    /**
     * Default constructor, which expects a string value as name for the
     * generator.
     *
     * @param name The name for the generator
     */
    public GeneratorDescription(String name, String classname) {
        super();
        setName(name);
        setClassname(classname);
    }

    /**
     * This function returns the classname of the generator implementation
     * as <code>String</code>.
     *
     * @return The classname of the generator implementation
     */
    public String getClassname() {
        return classname;
    }

    /**
     * This function sets the classname of the generator implementation
     *
     * @param classname The full qualified classname of the generator implementation
     */
    private void setClassname(String classname) {
        this.classname = classname;
    }

    /**
     * This function returns the given name for the generator as specified
     * either in the constructor or be using {@link #setName(String)}.
     *
     * @return The name of the generator
     * @see #setName(String)
     */
    public String getName() {
        return name;
    }

    /**
     * Use this function to change the name of the generator. If a null
     * value is given as name it is silently ignored.
     *
     * @param name The new name for the generator
     * @see #getName()
     */
    public void setName(String name) {
        if (name != null) {
            this.name = name;
        }
    }

    /**
     * Use this function to add a new property to the generators properties.
     *
     * @param key   The key name of the property
     * @param value The property value
     */
    public void addProperty(String key, String value) {
        properties.put(key, value);
    }

    /**
     * Removes the mapping for a key from this map if it is present (optional operation).
     * More formally, if this map contains a mapping from key k to value v, that mapping is removed.
     * (The map can contain at most one such mapping.)
     *
     * @param key The key for the property to remove
     * @return Returns the value to which this map previously associated the key, or null if the map contained no mapping for the key.
     */
    public String removeProperty(String key) {
        return properties.remove(key);
    }

    /**
     * Returns a Set view of the keys contained in this properties list. The set is backed by the map,
     * so changes to the map are reflected in the set, and vice-versa. If the map is modified
     * while an iteration over the set is in progress (except through the iterator's own remove
     * operation), the results of the iteration are undefined. The set supports element removal,
     * which removes the corresponding mapping from the map, via the Iterator.remove,
     * Set.remove, removeAll, retainAll, and clear operations. It does not support the add or
     * addAll operations.
     *
     * @return a set view of the property keys contained in the generator
     */
    public Set<String> getProperties() {
        return properties.keySet();
    }

    /**
     * Returns the value to which the specified key is mapped,
     * or null if this variant contains no variable for the key.
     *
     * @param name The key name of the property
     * @return The property value as string
     */
    public String getPropertyValue(String name) {
        return properties.get(name);
    }

    /**
     * Appends the specified element to the end of this list.
     *
     * @param name The name of the generator
     */
    public void addDependency(String name) {
        dependencies.add(name);
    }

    /**
     * Removes the first occurrence of the specified dependency from this list,
     * if it is present (optional operation). If this list does not contain
     * the dependency, it is unchanged.
     *
     * @param name The name of the dependent generator
     */
    public void removeDependency(String name) {
        dependencies.remove(name);
    }

    /**
     * This function returns the dependencies as string array.
     * The returned array will be "safe" in that there are no references to the underlying
     * generator description.
     *
     * @return String array containing the depending generator names
     */
    public Set<String> getDependencies() {
        return dependencies;
    }
}
