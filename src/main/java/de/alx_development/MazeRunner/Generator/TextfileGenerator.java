package de.alx_development.MazeRunner.Generator;

/*-
 * #%L
 * MazeRunner configuration management
 * %%
 * Copyright (C) 2019 ALX-Development
 * %%
 * This file is part of MazeRunner.
 *
 * MazeRunner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MazeRunner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil des MazeRunner Projekt.
 *
 * MazeRunner ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * MazeRunner wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import de.alx_development.MazeRunner.Configuration.Configuration;
import de.alx_development.MazeRunner.Generator.Checker.CheckerException;

import javax.swing.*;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Alexander Thiel
 */
public class TextfileGenerator extends Generator {

    /**
     * constructor
     */
    public TextfileGenerator() {
        super();

        /*
         * Defining the generator specific properties
         */
        try {
            addProperty("INPUT.filename", new URL("file://data/templates/TextfileTemplate.txt").toString());
            addProperty("OUTPUT.filename", new URL("file://data/templates/ResultTemplate.txt").toString());
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
    }

    /**
     * generates a result file for the given configuration
     *
     * @param configuration - the configuration to be generated
     */
    @Override
    public void generate(Configuration configuration) {

        String strLine;
        String strBoolExpression;
        String strPatternCommand = "^\\*\\[([a-zA-Z0-9_|&!-\\(\\)]*)\\]\\*";
        // String strPatternCommand = "^\\*\\[([" +
        // configuration.getValidCharsCommand()+ "|&!-\\(\\)]*)\\]\\*";
        Scanner scan;

        try {
            FileWriter f = new FileWriter(new URL(getPropertyValue("OUTPUT.filename")).getFile());
            Checker checkConfiguration = new Checker(configuration);
            // check all lines in template file against the configuration
            scan = new Scanner(new InputStreamReader(new URL(getPropertyValue("INPUT.filename")).openStream()));

            while (scan.hasNextLine()) {
                strLine = scan.nextLine();

                Pattern rCommand = Pattern.compile(strPatternCommand);

                Matcher mCommand = rCommand.matcher(strLine);

                strBoolExpression = "";
                while (mCommand.find()) {
                    // replace the embracing characters for a valid bool expression
                    strBoolExpression = mCommand.group(0).replace('*', ' ').replace('[', ' ').replace(']', ' ').trim();
                }

                try {
                    if (checkConfiguration.check(strBoolExpression)) {
                        strLine = setVariables(configuration, strLine);
                        strLine = strLine.replaceAll(strPatternCommand, "");
                        f.append(strLine).append("\n");
                    }
                } catch (CheckerException ex) {
                    JOptionPane.showMessageDialog(null, "fehlerhafter Ausdruck" + ex.getMessage(),
                            "InfoBox: Generieren nicht möglich ", JOptionPane.INFORMATION_MESSAGE);
                }

            }
            System.out.println("finished----------" + configuration.getComponentAt(0).getActiveVariant().getName());
            f.flush();
            f.close();

        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "" + ex.getMessage(), "InfoBox: Generieren nicht möglich ",
                    JOptionPane.INFORMATION_MESSAGE);
            // System.err.println(ex.getMessage());
        }
    }

    /**
     * function to replace variables with values from a configuration
     *
     * @param configuration - the configuration with the informations
     * @param line          - the line with variables to be replaced
     * @return - the line with replaced variables
     */
    private String setVariables(Configuration configuration, String line) {
        String strResult = line;
        Pattern rVariable = Pattern.compile("\\$(([a-zA-Z0-9.-]*)_?([a-zA-Z0-9.-]*))\\$");

        Matcher mVariable = rVariable.matcher(line);

        while (mVariable.find()) {
            if (configuration.getVariables().get(mVariable.group(1)) == null) {
                JOptionPane.showMessageDialog(null, "keinen Eintrag für " + mVariable.group(1),
                        "InfoBox: Generierung nicht vollständig ", JOptionPane.INFORMATION_MESSAGE);
                System.out.println("keinen Eintrag für " + mVariable.group(1));
            } else {
                strResult = strResult.replace(mVariable.group(0), configuration.getVariables().get(mVariable.group(1)));
            }
        }
        return strResult;
    }

}
