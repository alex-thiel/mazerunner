package de.alx_development.MazeRunner.Generator;

/*-
 * #%L
 * MazeRunner configuration management
 * %%
 * Copyright (C) 2019 ALX-Development
 * %%
 * This file is part of MazeRunner.
 *
 * MazeRunner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MazeRunner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil des MazeRunner Projekt.
 *
 * MazeRunner ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * MazeRunner wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import de.alx_development.MazeRunner.Configuration.Configuration;

import java.util.Set;

/**
 * @author Alexander Thiel
 */
public abstract class Generator {

    private GeneratorDescription description;

    /**
     * This is the most important function of each generator, it initiates
     * the generator process. Every time this function is called, the generator
     * begins working based upon the configuration and the configurators own properties.
     *
     * @param configuration The configuration which defines the configuration parameters
     */
    public abstract void generate(Configuration configuration);

    /**
     * This function returns the given name for the generator as specified
     * either in the constructor or be using {@link #setName(String)}.
     *
     * @return The name of the generator
     * @see #setName(String)
     */
    public String getName() {
        return getDescription().getName();
    }

    /**
     * Use this function to change the name of the generator. If a null
     * value is given as name it is silently ignored.
     *
     * @param name The new name for the generator
     * @see #getName()
     */
    public void setName(String name) {
        if (name != null) {
            getDescription().setName(name);
        }
    }

    /**
     * The description object is the representation which is stored inside configurations.
     *
     * @return The generator description object
     */
    protected GeneratorDescription getDescription() {
        /*
         * If the generator description is not yet initialized, it will be at the
         * first time it is requested
         */
        if (description == null) {
            description = new GeneratorDescription("undefined", this.getClass().getName());
        }

        return description;
    }

    /**
     * This function is used by the internal generator factory to extract the description
     * from a generator instance.
     * If the given parameter is a null value or the class definition of the description
     * does not match to the class of the generator an <code>IllegalArgumentException</code> is
     * thrown.
     *
     * @param description The description to use with this generator
     */
    protected void setDescription(GeneratorDescription description) {
        /*
         * Checking if the given description is a null value. Null values
         * are not allowed as parameter and therefore ignored.
         */
        if (description == null) {
            throw new IllegalArgumentException("Generator description must not be a null value");
        }

        /*
         * Checking if the class definition is the same in the description and the generator
         */
        if (!description.getClassname().equals(getClass().getName())) {
            throw new IllegalArgumentException("Generator " + getClass().getSimpleName() + " is not compatible to class [" + getClass().getName() + "]");
        }

        this.description = description;
    }

    /**
     * Use this function to add a new property to the generators properties.
     *
     * @param key   The key name of the property
     * @param value The property value
     */
    public void addProperty(String key, String value) {
        getDescription().addProperty(key, value);
    }

    /**
     * Removes the mapping for a key from this map if it is present (optional operation).
     * More formally, if this map contains a mapping from key k to value v, that mapping is removed.
     * (The map can contain at most one such mapping.)
     *
     * @param key The key for the property to remove
     * @return Returns the value to which this map previously associated the key, or null if the map contained no mapping for the key.
     */
    public String removeProperty(String key) {
        return getDescription().removeProperty(key);
    }

    /**
     * Returns a Set view of the keys contained in this properties list. The set is backed by the map,
     * so changes to the map are reflected in the set, and vice-versa. If the map is modified
     * while an iteration over the set is in progress (except through the iterator's own remove
     * operation), the results of the iteration are undefined. The set supports element removal,
     * which removes the corresponding mapping from the map, via the Iterator.remove,
     * Set.remove, removeAll, retainAll, and clear operations. It does not support the add or
     * addAll operations.
     *
     * @return a set view of the property keys contained in the generator
     */
    public Set<String> getProperties() {
        return getDescription().getProperties();
    }

    /**
     * Returns the value to which the specified key is mapped,
     * or null if this variant contains no variable for the key.
     *
     * @param name The key name of the property
     * @return The property value as string
     */
    public String getPropertyValue(String name) {
        return getDescription().getPropertyValue(name);
    }
}
