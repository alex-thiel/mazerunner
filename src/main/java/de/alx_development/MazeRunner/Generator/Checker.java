package de.alx_development.MazeRunner.Generator;

/*-
 * #%L
 * MazeRunner configuration management
 * %%
 * Copyright (C) 2019 ALX-Development
 * %%
 * This file is part of MazeRunner.
 *
 * MazeRunner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MazeRunner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil des MazeRunner Projekt.
 *
 * MazeRunner ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * MazeRunner wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import com.bpodgursky.jbool_expressions.Expression;
import com.bpodgursky.jbool_expressions.parsers.ExprParser;
import com.bpodgursky.jbool_expressions.rules.RuleSet;
import de.alx_development.MazeRunner.Configuration.Configuration;

import java.util.Collections;

/**
 * class to check boolean expressions against a configuration
 *
 * @author Alexander Thiel
 */
public class Checker {

    private final Configuration configuration;

    /**
     * constructor
     *
     * @param configuration The configuration which is used as command base for the checker
     */
    public Checker(Configuration configuration) {
        this.configuration = configuration;
    }

    /**
     * function to check whether a boolean expression is true or false the terms are
     * replaced with true if found in the configuration
     *
     * @param expression the expression to be checked
     * @return true if expression is true, false if expression is false or not every
     * term is replaced by true or false
     * @throws CheckerException The exception is thrown if something ent wrong during check
     */
    public boolean check(String expression) throws CheckerException {
        // if there is no expression it is valid for all configurations
        if (expression.isEmpty()) {
            return true;
        }
        try {
            Expression<String> parsedExpression = RuleSet.simplify(ExprParser.parse(expression));

            for (int i = 0; i < configuration.getCommands().size(); i++) {
                parsedExpression = RuleSet.assign(parsedExpression,
                        Collections.singletonMap(configuration.getCommands().get(i), true));
            }
            // TODO: aus irgendwelchen Gründen ist hier die Zwischenspeicherung in strResult notwendig!
            String strResult = parsedExpression.toLexicographicString();
            return Boolean.parseBoolean(strResult);
        } catch (Exception e) {
            throw new CheckerException(e.getMessage());
        }
    }

    static class CheckerException extends Exception {

        /**
         *
         */
        private static final long serialVersionUID = -1761358695328526210L;

        CheckerException(String strMessage) {
            super("Checker: " + strMessage);
        }
    }
}
