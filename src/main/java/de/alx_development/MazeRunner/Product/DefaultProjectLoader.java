package de.alx_development.MazeRunner.Product;

/*-
 * #%L
 * MazeRunner configuration management
 * %%
 * Copyright (C) 2019 ALX-Development
 * %%
 * This file is part of MazeRunner.
 *
 * MazeRunner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MazeRunner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil des MazeRunner Projekt.
 *
 * MazeRunner ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * MazeRunner wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

/**
 * WARNING: It's just a simple dummy loader not really loading the content from
 * a file or database. The content is build by the loader itself. Just use this
 * to test the user interface implementation.
 *
 * @author Alexander Thiel
 */
public class DefaultProjectLoader implements ProjectLoader {

    /**
     *
     */
    public DefaultProjectLoader() {
        // TODO Auto-generated constructor stub
    }

    @Override
    public void load(Project project, String name) {
        // TODO Auto-generated method stub

        project.setProperty("project.name", name);
        project.setProperty("project.projectid", "12345");
        project.setProperty("project.customer.name", "The Usercompany");
        project.setProperty("project.customer.country", "Aserbaidschan");

        Product product = new Product();
        product.setProperty("product.serialnumber", "123456");
        product.setProperty("product.materialnumber", "108055022");
        product.setProperty("product.type", "Transportrollenbahn");
        project.addProduct("50000", product);

        Feature feature1 = new Feature();
        feature1.setProperty("feature.number", "10");
        feature1.setProperty("feature.description", "Rollenantrieb, geregelt, 1kW");
        feature1.setProperty("feature.class", "Klasse");
        feature1.setProperty("feature.financial.id", "800126");
        product.addFeature(feature1);

        Feature feature2 = new Feature();
        feature2.setProperty("feature.description", "Rollenabstand < 60mm");
        feature2.setProperty("feature.class", "Klasse");
        feature2.setProperty("feature.financial.id", "800168");
        product.addFeature(feature2);

        Feature feature3 = new Feature();
        feature3.setProperty("feature.number", "20");
        feature3.setProperty("feature.description", "Rollendurchmesser 80mm");
        feature3.setProperty("feature.class", "Klasse");
        feature3.setProperty("feature.financial.id", "800774");
        product.addFeature(feature3);

        Feature feature4 = new Feature();
        feature4.setProperty("feature.number", "30");
        feature4.setProperty("feature.description", "Transportgeschwindigkeit 10 - 30 m/s");
        feature4.setProperty("feature.class", "Klasse");
        feature4.setProperty("feature.financial.id", "800683");
        product.addFeature(feature4);

        Product product2 = new Product();
        product2.setProperty("product.serialnumber", "654987");
        product2.setProperty("product.materialnumber", "108055023");
        product2.setProperty("product.type", "Verpackungsmaschine");
        project.addProduct("51000", product2);

        Feature feature11 = new Feature();
        feature11.setProperty("feature.number", "20");
        feature11.setProperty("feature.description", "KompBenennung");
        feature11.setProperty("feature.class", "Klasse");
        feature11.setProperty("feature.financial.id", "800126");
        product2.addFeature(feature11);

        Feature featrue12 = new Feature();
        featrue12.setProperty("feature.number", "20");
        featrue12.setProperty("feature.description", "KompBenennung");
        featrue12.setProperty("feature.class", "Klasse");
        featrue12.setProperty("feature.financial.id", "800126");
        product2.addFeature(featrue12);

        Feature feature13 = new Feature();
        feature13.setProperty("feature.number", "20");
        feature13.setProperty("feature.description", "KompBenennung");
        feature13.setProperty("feature.class", "Klasse");
        feature13.setProperty("feature.financial.id", "800126");
        product2.addFeature(feature13);
    }

}
