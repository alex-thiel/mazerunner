package de.alx_development.MazeRunner.Product;

/*-
 * #%L
 * MazeRunner configuration management
 * %%
 * Copyright (C) 2019 ALX-Development
 * %%
 * This file is part of MazeRunner.
 *
 * MazeRunner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MazeRunner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil des MazeRunner Projekt.
 *
 * MazeRunner ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * MazeRunner wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import javax.swing.*;
import java.awt.*;

/**
 * This class is used as UI representation of a feature
 * list carried by a product.
 *
 * @author Alexander Thiel
 */
public class FeatureListView extends JComponent {

    /**
     *
     */
    private static final long serialVersionUID = 5172974136107101727L;
    private final JList<Feature> featureList;


    /**
     * The default constructor is expecting building the UI, but
     * without an underlying model. To add the model to display
     * use the {@link #setModel(FeatureListModel)} method.
     */
    public FeatureListView() {
        super();

        featureList = new JList<>();
        featureList.setCellRenderer(new FeatureListCellRenderer());
        featureList.setLayout(new BorderLayout());

        JScrollPane listScrollPane = new JScrollPane(featureList, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        setLayout(new BorderLayout());
        add(listScrollPane, BorderLayout.CENTER);
    }

    /**
     * Sets the model that represents the contents or "value" of the list,
     * notifies property change listeners, and then clears the list's selection.
     * This is a JavaBeans bound property.
     *
     * @param model The ListModel that provides the list of items for display
     * @throws IllegalArgumentException - if model is null
     */
    public void setModel(FeatureListModel model) {
        featureList.setModel(model);
    }
}
