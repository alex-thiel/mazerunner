package de.alx_development.MazeRunner.Product;

/*-
 * #%L
 * MazeRunner configuration management
 * %%
 * Copyright (C) 2019 ALX-Development
 * %%
 * This file is part of MazeRunner.
 *
 * MazeRunner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MazeRunner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil des MazeRunner Projekt.
 *
 * MazeRunner ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * MazeRunner wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import java.util.Enumeration;
import java.util.Properties;

/**
 * @author alex
 */
public class ProductPropertiesContainer {

    private final Properties properties = new Properties();

    /**
     *
     */
    public ProductPropertiesContainer() {
        super();
    }

    /**
     * Associate value with key. Returns the previous value associated with key,
     * or returns null if no such association exist.
     *
     * @param key   The property key
     * @param value The new value for the property.
     * @return The previous value of the specified key in this property list, or null if it did not have one.
     */
    public String setProperty(String key, String value) {
        return (String) this.properties.setProperty(key, value);
    }

    /**
     * This method returns an enumeration of the keys.
     *
     * @return Enumeration of available keys
     */
    public Enumeration<String> getPropertyNames() {
        @SuppressWarnings("unchecked")
        Enumeration<String> propertyNames = (Enumeration<String>) properties.propertyNames();
        return propertyNames;
    }

    /**
     * Searches for the property with the specified key in this property list.
     * If the key is not found in this property list, the default property list,
     * and its defaults, recursively, are then checked.
     * The method returns null if the property is not found.
     *
     * @param key The property key to look for.
     * @return The value associated with the key.
     */
    public String getProperty(String key) {
        return this.properties.getProperty(key);
    }

    /**
     * Searches for the property with the specified key in this property list.
     * If the key is not found in this property list, the default property list,
     * and its defaults, recursively, are then checked.
     * The method returns the default value argument if the property is not found.
     * In difference to the java implementation of a properties list the default value
     * is associated with the key and stored permanently in the properties list.
     *
     * @param key          The property key to look for.
     * @param defaultValue The default property value is returned, if property is not already available
     * @return The value associated with key.
     */
    public String getProperty(String key, String defaultValue) {
        /*
         * The default property is set permanently if a not null value
         * has been assigned.
         */
        if (defaultValue != null) {
            this.setProperty(key, defaultValue);
        }
        return this.properties.getProperty(key, defaultValue);
    }

}
