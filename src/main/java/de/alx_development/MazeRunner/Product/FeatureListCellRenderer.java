package de.alx_development.MazeRunner.Product;

/*-
 * #%L
 * MazeRunner configuration management
 * %%
 * Copyright (C) 2019 ALX-Development
 * %%
 * This file is part of MazeRunner.
 *
 * MazeRunner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MazeRunner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil des MazeRunner Projekt.
 *
 * MazeRunner ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * MazeRunner wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import javax.swing.*;
import java.awt.*;

/**
 * @author Alexander Thiel
 */
public class FeatureListCellRenderer extends DefaultListCellRenderer {

    /**
     *
     */
    private static final long serialVersionUID = -4060825533568290769L;

    public FeatureListCellRenderer() {
        super();
    }

    /**
     * @param list         - the list of the position
     * @param value        - the position to show
     * @param index        -the index of the cell
     * @param isSelected   - is the cell selected
     * @param cellHasFocus - has the cell the focus
     * @return - the component to show
     */
    @Override
    public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected,
                                                  boolean cellHasFocus) {

        /*
         * Initializing the super class which is extended by this renderer
         */
        super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);

        /*
         * Setting up the text representation for the list element
         */
        Feature feature = (Feature) value;
        FeatureListModel model = (FeatureListModel) list.getModel();

        setText(String.format("<html><body><b>%s</b> %s</body></html>",
                model.getElementAt(index).getProperty("feature.number"),
                feature.getProperty("feature.description")));

        return this;
    }
}
