package de.alx_development.MazeRunner.Product;

/*-
 * #%L
 * MazeRunner configuration management
 * %%
 * Copyright (C) 2019 ALX-Development
 * %%
 * This file is part of MazeRunner.
 *
 * MazeRunner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MazeRunner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil des MazeRunner Projekt.
 *
 * MazeRunner ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * MazeRunner wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import javax.swing.*;
import java.awt.*;

/**
 * class to handle a list for an order card
 *
 * @author Alexander Thiel
 */
public class ProjectView extends JComponent {

    /**
     *
     */
    private static final long serialVersionUID = 6793842759651488527L;
    private final JList<Product> productlist;
    private final JList<Feature> featurelist;
    private final ProductPropertiesView properties;

    /**
     * Constructor creates the elements
     */
    public ProjectView() {
        super();

        /*
         * Initializing the visual elements
         */
        productlist = new JList<>();
        productlist.setCellRenderer(new ProductListCellRenderer());
        productlist.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        productlist.addListSelectionListener(event -> {
            JList<Product> list = (JList<Product>) event.getSource();
            ListSelectionModel lsm = list.getSelectionModel();
            setPropertiesViewElement("Product",
                    productlist.getModel().getElementAt(lsm.getMinSelectionIndex()));
            setFeatureListProduct(productlist.getModel().getElementAt(lsm.getMinSelectionIndex()));
        });

        JScrollPane productlistScrollPane = new JScrollPane(productlist,
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        featurelist = new JList<>();
        featurelist.setCellRenderer(new FeatureListCellRenderer());
        featurelist.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        JScrollPane featurelistScrollPane = new JScrollPane(featurelist,
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        properties = new ProductPropertiesView();

        JSplitPane splitPaneVert1 = new JSplitPane(JSplitPane.VERTICAL_SPLIT, properties, productlistScrollPane);
        splitPaneVert1.setOneTouchExpandable(true);
        splitPaneVert1.setResizeWeight(1.0);
        //splitPaneVert1.setDividerLocation(280);

        JSplitPane splitPaneVert2 = new JSplitPane(JSplitPane.VERTICAL_SPLIT, splitPaneVert1, featurelistScrollPane);
        splitPaneVert2.setOneTouchExpandable(true);
        splitPaneVert2.setResizeWeight(1.0);
        //splitPaneVert2.setDividerLocation(80);

        setLayout(new BorderLayout());
        add(splitPaneVert2, BorderLayout.CENTER);
    }

    /**
     * This function should be called to set the project which has to be represented
     * by this visual component.
     *
     * @param project The <code>Project</code> object which should be displayed
     */
    public void setProject(Project project) {
        productlist.setModel(new ProductListModel(project));
        properties.setPropertiesContainer("Project", project);
    }

    private void setPropertiesViewElement(String name, ProductPropertiesContainer container) {
        properties.setPropertiesContainer(name, container);
    }

    private void setFeatureListProduct(Product product) {
        FeatureListModel model = new FeatureListModel(product);
        featurelist.setModel(model);
    }
}
