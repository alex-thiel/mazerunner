package de.alx_development.MazeRunner.Product;

/*-
 * #%L
 * MazeRunner configuration management
 * %%
 * Copyright (C) 2019 ALX-Development
 * %%
 * This file is part of MazeRunner.
 *
 * MazeRunner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MazeRunner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil des MazeRunner Projekt.
 *
 * MazeRunner ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * MazeRunner wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import javax.swing.*;
import javax.swing.text.EditorKit;
import javax.swing.text.html.HTMLEditorKit;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * @author Alexander Thiel
 */
public class CatalogueViewer extends JPanel {
    private static final long serialVersionUID = 4871188910521000690L;

    private final JTextPane jTextPane = new JTextPane();

    public CatalogueViewer(String position) {

        // EditorKit setzen
        EditorKit eKit = new HTMLEditorKit();
        jTextPane.setEditorKit(eKit);
        setPosition(position);

        // Größe des Scrollpanes bestimmen
        JScrollPane jScrollPane = new JScrollPane();
        jScrollPane.setBounds(new Rectangle(40, 30, 150, 50));
        // JTExtPane dem Scrollpane hinzufügen
        jScrollPane.getViewport().add(jTextPane, null);

        // ScrollPane dem contentPane hinzufügen
        setLayout(new BorderLayout());
        add(jScrollPane, BorderLayout.CENTER);
    }

    public void setPosition(String position) {
        try {
            // TODO: Must be implemented configurable
            URL url = new File("data/catalogue", position + ".htm").getAbsoluteFile().toURI().toURL();
            jTextPane.setPage(url);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
