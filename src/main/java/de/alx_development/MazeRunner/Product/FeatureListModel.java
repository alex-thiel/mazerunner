package de.alx_development.MazeRunner.Product;

/*-
 * #%L
 * MazeRunner configuration management
 * %%
 * Copyright (C) 2019 ALX-Development
 * %%
 * This file is part of MazeRunner.
 *
 * MazeRunner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MazeRunner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil des MazeRunner Projekt.
 *
 * MazeRunner ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * MazeRunner wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import javax.swing.*;
import javax.swing.event.ListDataListener;

/**
 * @author Alexander Thiel
 */
public class FeatureListModel implements ListModel<Feature> {

    /**
     * The reference to the product which is represented by this model
     */
    private Product product;

    /**
     * Constructor without any parameter. Use
     * <code>setProduct(Product product)</code> to set the list which should
     * be handled by this model.
     */
    public FeatureListModel() {
        super();
    }

    /**
     * Constructor wit parameter. Pass the <code>Product</code> reference to set
     * the list which should be handled by this model.
     *
     * @param product - The product which feature structure should be handled by this model
     */
    public FeatureListModel(Product product) {
        super();
        setProduct(product);
    }


    public void setProduct(Product product) {
        this.product = product;
    }

    @Override
    public Feature getElementAt(int arg0) {
        return product.getFeature(arg0);
    }

    /**
     * Returns the length of the list.
     *
     * @return the length of the list
     */
    @Override
    public int getSize() {
        return product.getFeatureCount();
    }

    @Override
    public void addListDataListener(ListDataListener arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void removeListDataListener(ListDataListener arg0) {
        // TODO Auto-generated method stub

    }
}
