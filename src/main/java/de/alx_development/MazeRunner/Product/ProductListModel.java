package de.alx_development.MazeRunner.Product;

/*-
 * #%L
 * MazeRunner configuration management
 * %%
 * Copyright (C) 2019 ALX-Development
 * %%
 * This file is part of MazeRunner.
 *
 * MazeRunner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MazeRunner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil des MazeRunner Projekt.
 *
 * MazeRunner ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * MazeRunner wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import javax.swing.*;
import javax.swing.event.EventListenerList;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

/**
 * @author Alexander Thiel
 */
public class ProductListModel implements ListModel<Product> {

    /**
     * The reference to the project which is represented by this model
     */
    protected Project project;

    /**
     * List of listeners
     */
    protected EventListenerList listeners = new EventListenerList();

    /**
     * The model constructor is expecting the <code>Project</code> which
     * should be handled by this model.
     */
    public ProductListModel(Project project) {
        super();
        if (project == null) {
            throw new IllegalArgumentException(
                    "Null pointer references not allowed as root for the Project list model");
        }
        this.project = project;
    }

    /**
     * Returns the length of the list.
     *
     * @return the length of the list
     */
    @Override
    public int getSize() {
        return project.getProductCount();
    }

    /**
     * This function returns the element at the given index.
     * Notice, the index is not equal to the product key which is
     * used to identify products inside a project.
     *
     * @param index The list index of the element
     * @return The <code>Product</code> stored at the index of the list
     */
    @Override
    public Product getElementAt(int index) {
        String key = project.getProductKey(index);
        return project.getProduct(key);
    }

    /**
     * This function is called every time a change in the list occurs.
     *
     * @param source The object which fires the change event
     */
    protected void fireListStructureChanged(Object source) {
        // Guaranteed to return a non-null array
        Object[] listenerList = listeners.getListenerList();
        ListDataEvent e = new ListDataEvent(source, ListDataEvent.CONTENTS_CHANGED, 0, getSize());
        // Process the listeners last to first, notifying
        // those that are interested in this event
        for (int i = listenerList.length - 2; i >= 0; i -= 2) {
            ((ListDataListener) listenerList[i + 1]).contentsChanged(e);
        }
    }

    /**
     * Adds a listener to the list that's notified each time a change to the data
     * model occurs.
     *
     * @param l - the ListDataListener to be added
     */
    @Override
    public synchronized void addListDataListener(ListDataListener l) {
        listeners.add(ListDataListener.class, l);
    }

    /**
     * Removes a listener from the list that's notified each time a change to the
     * data model occurs.
     *
     * @param l - the ListDataListener to be removed
     */
    @Override
    public synchronized void removeListDataListener(ListDataListener l) {
        listeners.remove(ListDataListener.class, l);
    }

}
