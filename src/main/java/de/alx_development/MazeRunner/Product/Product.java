package de.alx_development.MazeRunner.Product;

/*-
 * #%L
 * MazeRunner configuration management
 * %%
 * Copyright (C) 2019 ALX-Development
 * %%
 * This file is part of MazeRunner.
 *
 * MazeRunner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MazeRunner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil des MazeRunner Projekt.
 *
 * MazeRunner ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * MazeRunner wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import java.util.ArrayList;

/**
 * @author Alexander Thiel
 */
public class Product extends ProductPropertiesContainer {

    private final ArrayList<Feature> features = new ArrayList<>();

    public Product() {
        super();
    }

    /**
     * Appends the specified element to the end of this list.
     *
     * @param feature The feature object to add.
     */
    public void addFeature(Feature feature) {
        features.add(feature);
    }

    /**
     * Returns the element at the specified position in this feature list.
     *
     * @param idx The list index of the feature to be returned.
     * @return The feature element at the specified position in this list
     */
    public Feature getFeature(int idx) {
        return features.get(idx);
    }

    /**
     * Returns the number of elements in this list.
     *
     * @return The number of elements in this feature list
     */
    public int getFeatureCount() {
        return features.size();
    }

    /**
     * Removes the element at the specified position in this list. Shifts any subsequent
     * elements to the left (subtracts one from their indices).
     *
     * @param idx Feature element to be removed from this list, if present
     * @return The feature element that was removed from the list
     */
    public Feature removeFeature(int idx) {
        return features.remove(idx);
    }
}
