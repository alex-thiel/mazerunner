package de.alx_development.MazeRunner.Product;

/*-
 * #%L
 * MazeRunner configuration management
 * %%
 * Copyright (C) 2019 ALX-Development
 * %%
 * This file is part of MazeRunner.
 *
 * MazeRunner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MazeRunner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil des MazeRunner Projekt.
 *
 * MazeRunner ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * MazeRunner wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import java.util.HashMap;
import java.util.Set;

/**
 * A project is used to combine several products to one larger project. This
 * approach is often used in larger projects where to combine smaller products
 * to larger one.
 *
 * @author Alexander Thiel
 */
public class Project extends ProductPropertiesContainer {

    private HashMap<String, Product> products = new HashMap<>();

    /**
     * Standard constructor without any parameter. If a name should be defined
     * for the project, use {@link #setProperty(String, String)} using <code>project.name</code>
     * as key.
     */
    public Project() {
        this("undefined");
    }

    /**
     * The default constructor is expecting a string as name for the project. If the
     * project has been loaded from a file system often the file name is also used
     * as project name.
     *
     * @param name The name for the project
     */
    public Project(String name) {
        super();
        setProperty("project.name", name);
    }

    /**
     * This function adds a new <code>Product</code> reference to the
     * project. It is usual to use named position keys (usually numbers)
     * in a project to identify and sort the project content.
     * <p>
     * If the product is already listed (independent from the used key)
     * an <code>IllegalArgumentException</code> is thrown.
     *
     * @param key     The key for the product to add
     * @param product The product reference
     */
    public void addProduct(String key, Product product) {
        if (products.containsValue(product)) {
            throw new IllegalArgumentException("Product already listed in project.");
        }

        products.put(key, product);
    }

    /**
     * This function returns a set of all product keys available in this
     * project. Every key is assigned to a product.
     *
     * @return The key list
     */
    public Set<String> getProductKeys() {
        return products.keySet();
    }

    /**
     * This function returns the key for the product currently stored inside
     * the list at the given index.
     *
     * @param index The list index where the product is stored
     * @return The <code>String</code> representation of the product key
     */
    public String getProductKey(int index) {
        Object[] keys = products.keySet().toArray();
        return keys[index].toString();
    }

    /**
     * Returns a reference to the <code>Product</code> with the given
     * key as reference.
     *
     * @param key The key which identifies the product
     * @return product A reference to the <code>Product</code> stored assigned to the given key
     */
    public Product getProduct(String key) {
        return products.get(key);
    }

    /**
     * This function returns the number of <code>Product</code>s stored referenced
     * in this project.
     *
     * @return The number of products
     */
    public int getProductCount() {
        return products.size();
    }

    /**
     * Use this function to remove a previously added product from the project.
     *
     * @param key The key used to store the product which should be removed
     */
    public void removeProduct(String key) {
        products.remove(key);
    }
}
