package de.alx_development.MazeRunner.BillOfMaterial;

/*-
 * #%L
 * MazeRunner configuration management
 * %%
 * Copyright (C) 2019 ALX-Development
 * %%
 * This file is part of MazeRunner.
 *
 * MazeRunner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MazeRunner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil des MazeRunner Projekt.
 *
 * MazeRunner ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * MazeRunner wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import java.util.EventObject;

/**
 * An event that contains information about a modification to the content of
 * a bill of material.
 *
 * @author Alexander Thiel
 */
public class BillOfMaterialChangeEvent extends EventObject {

    /**
     * An event type indicating that the list content has been modified.
     */
    public static final int CONTENTS_CHANGED = 0;
    /**
     * An event type indicating that an interval has been added to the list.
     */
    public static final int INTERVAL_ADDED = 1;
    /**
     * An event type indicating that an interval has been removed from the list.
     */
    public static final int INTERVAL_REMOVED = 2;
    private static final long serialVersionUID = 2606337411256019986L;
    private final int type;
    private final int index0;
    private final int index1;

    /**
     * Creates a <code>BillOfMaterialChangeEvent</code> object.
     *
     * @param source the <code>BillOfMaterial</code> of the event (<code>null</code> not permitted).
     * @param type   the type of the event (should be one of {@link #CONTENTS_CHANGED}, {@link #INTERVAL_ADDED} or {@link #INTERVAL_REMOVED}, although this is not enforced).
     * @param index0 the index for one end of the modified range of list elements.
     * @param index1 the index for the other end of the modified range of list elements.
     */
    public BillOfMaterialChangeEvent(BillOfMaterial source, int type, int index0, int index1) {
        super(source);
        this.type = type;
        this.index0 = Math.min(index0, index1);
        this.index1 = Math.max(index0, index1);
    }

    /**
     * Returns the index of the first item in the range of modified list items.
     *
     * @return The index of the first item in the range of modified list items.
     */
    public int getIndex0() {
        return index0;
    }

    /**
     * Returns the index of the last item in the range of modified list items.
     *
     * @return The index of the last item in the range of modified list items.
     */
    public int getIndex1() {
        return index1;
    }

    /**
     * Returns a code representing the type of this event, which is usually one
     * of {@link #CONTENTS_CHANGED}, {@link #INTERVAL_ADDED} or {@link #INTERVAL_REMOVED}.
     *
     * @return The event type.
     */
    public int getType() {
        return type;
    }

    /**
     * Returns a string representing the state of this event.
     *
     * @return A string.
     */
    public String toString() {
        return getClass().getName() + "[type=" + type + ",index0=" + index0 + ",index1=" + index1 + "]";
    }
}
