package de.alx_development.MazeRunner.BillOfMaterial;

/*-
 * #%L
 * MazeRunner configuration management
 * %%
 * Copyright (C) 2019 ALX-Development
 * %%
 * This file is part of MazeRunner.
 *
 * MazeRunner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MazeRunner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil des MazeRunner Projekt.
 *
 * MazeRunner ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * MazeRunner wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import javax.swing.*;
import javax.swing.event.EventListenerList;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

/**
 * class to handle a bill of material in list context
 *
 * @author Alexander Thiel
 */
public class BillOfMaterialListModel implements ListModel<BillOfMaterialElement>, BillOfMaterialChangeListener {

    /**
     * The reference to the BOM which is represented by this model
     */
    protected BillOfMaterial bom;

    /**
     * List of listeners
     */
    protected EventListenerList listeners = new EventListenerList();

    /**
     * Constructor without any parameter. Use <code>setBillOfMaterial(BillOfMaterial bom)</code>
     * to set the list which should be handled by this model.
     */
    public BillOfMaterialListModel() {
    }

    /**
     * Constructor wit parameter. Pass the <code>BillOfMaterial</code> reference
     * to set the list which should be handled by this model.
     *
     * @param bom - bill of material
     */
    public BillOfMaterialListModel(BillOfMaterial bom) {
        setBillOfMaterial(bom);
    }

    /**
     * This function allows access to the root bill of material
     * in this list modell
     *
     * @return the root bill of material
     */
    public BillOfMaterial getBillOfMaterial() {
        return this.bom;
    }

    /**
     * Use this method to pass the <code>BillOfMaterial</code> reference
     * to set the list which should be handled by this model.
     *
     * @param bom - bill of material
     */
    public void setBillOfMaterial(BillOfMaterial bom) {

        if (bom == null) {
            throw new UnsupportedOperationException("Null pointer references not allowed as root for BillOfMaterialListModel");
        }

        // Remove this list as change listener before removing the reference
        if (this.bom != null) {
            this.bom.removeBillOfMaterialChangeListener(this);
        }

        bom.addBillOfMaterialChangeListener(this);
        this.bom = bom;
        fireListStructureChanged(this);
    }

    /**
     * Returns the length of the list.
     *
     * @return the length of the list
     */
    @Override
    public int getSize() {
        return bom.getSize();
    }

    /**
     * Returns the value at the specified index.
     *
     * @param index - the requested index
     * @return the value at index
     */
    @Override
    public BillOfMaterialElement getElementAt(int index) {
        return bom.getElementAt(index);
    }

    /**
     * This function is called every time a change in the list occurs.
     *
     * @param source The object which fired the change event
     */
    protected void fireListStructureChanged(Object source) {
        // Guaranteed to return a non-null array
        Object[] listenerList = listeners.getListenerList();
        ListDataEvent e = new ListDataEvent(source, ListDataEvent.CONTENTS_CHANGED, 0, getSize());
        // Process the listeners last to first, notifying
        // those that are interested in this event
        for (int i = listenerList.length - 2; i >= 0; i -= 2) {
            ((ListDataListener) listenerList[i + 1]).contentsChanged(e);
        }
    }

    protected void fireBillOfMaterialChanged(BillOfMaterialChangeEvent event) {
        // Guaranteed to return a non-null array
        Object[] listenerList = listeners.getListenerList();
        ListDataEvent e = new ListDataEvent(event.getSource(), event.getType(), event.getIndex0(), event.getIndex1());
        // Process the listeners last to first, notifying
        // those that are interested in this event
        for (int i = listenerList.length - 2; i >= 0; i -= 2) {
            ((ListDataListener) listenerList[i + 1]).contentsChanged(e);
        }
    }

    @Override
    public void contentsChanged(BillOfMaterialChangeEvent event) {
        fireBillOfMaterialChanged(event);
    }

    @Override
    public void intervalAdded(BillOfMaterialChangeEvent event) {
        fireBillOfMaterialChanged(event);
    }

    @Override
    public void intervalRemoved(BillOfMaterialChangeEvent event) {
        fireBillOfMaterialChanged(event);
    }

    /**
     * Adds a listener to the list that's notified each time a change to the data model occurs.
     *
     * @param l - the ListDataListener to be added
     */
    @Override
    public synchronized void addListDataListener(ListDataListener l) {
        listeners.add(ListDataListener.class, l);
    }

    /**
     * Removes a listener from the list that's notified each time a change to the data model occurs.
     *
     * @param l - the ListDataListener to be removed
     */
    @Override
    public synchronized void removeListDataListener(ListDataListener l) {
        listeners.remove(ListDataListener.class, l);
    }
}
