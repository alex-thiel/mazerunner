package de.alx_development.MazeRunner.BillOfMaterial;

/*-
 * #%L
 * MazeRunner configuration management
 * %%
 * Copyright (C) 2019 ALX-Development
 * %%
 * This file is part of MazeRunner.
 *
 * MazeRunner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MazeRunner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil des MazeRunner Projekt.
 *
 * MazeRunner ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * MazeRunner wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import javax.swing.event.EventListenerList;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

/**
 * @author Alexander Thiel
 */
public class BillOfMaterialTreeModel implements TreeModel {

    /**
     * List of listeners
     */
    protected EventListenerList listenerList = new EventListenerList();
    protected BillOfMaterialElement root;

    /**
     *
     */
    public BillOfMaterialTreeModel(BillOfMaterialElement root) {
        setRoot(root);
    }

    @Override
    public BillOfMaterialElement getRoot() {
        return root;
    }

    /**
     * This method changes the root for the model. All TreeModelListener are
     * notified that the structure has changed.
     *
     * @param root The root element fo the tree model
     */
    public void setRoot(BillOfMaterialElement root) {
        BillOfMaterialElement oldRoot = this.root;
        this.root = root;
        if (root == null && oldRoot != null) {
            fireTreeStructureChanged(this, null);
        } else {
            nodeStructureChanged(root);
        }
    }

    @Override
    public BillOfMaterialElement getChild(Object parent, int index) {
        BillOfMaterialElement positionelement = (BillOfMaterialElement) parent;
        return positionelement.getMaterial().getElementAt(index);
    }

    @Override
    public int getChildCount(Object parent) {
        BillOfMaterialElement positionelement = (BillOfMaterialElement) parent;
        return positionelement.getMaterial().getSize();
    }

    @Override
    public boolean isLeaf(Object node) {
        return getChildCount(node) == 0;
    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {
        // TODO Auto-generated method stub

    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        // TODO Auto-generated method stub
        return 0;
    }

    /**
     * Invoke this method if you've totally changed the children of node and its
     * children's children... This will post a treeStructureChanged event.
     */
    public void nodeStructureChanged(BillOfMaterialElement node) {
        if (node != null) {
            // TODO:
            // fireTreeStructureChanged(this, getPathToRoot(node), null, null);
        }
    }

// TODO: Recursive backward resolving must be implemented
//	/**
//	 * Builds the parents of node up to and including the root node,
//	 * where the original node is the last element in the returned array.
//	 * The length of the returned array gives the node's depth in the
//	 * tree.
//	 * 
//	 * @param aNode the TreeNode to get the path for
//	 */
//	public BillOfMaterialElement[] getPathToRoot(BillOfMaterialElement aNode)
//	{
//		return getPathToRoot(aNode, 0);
//	}
//	
//	/**
//	 * Builds the parents of node up to and including the root node,
//	 * where the original node is the last element in the returned array.
//	 * The length of the returned array gives the node's depth in the
//	 * tree.
//	 * 
//	 * @param aNode  the TreeNode to get the path for
//	 * @param depth  an int giving the number of steps already taken towards
//	 *        the root (on recursive calls), used to size the returned array
//	 * @return an array of TreeNodes giving the path from the root to the
//	 *         specified node 
//	 */
//	protected BillOfMaterialElement[] getPathToRoot(Material aNode, int depth)
//	{
//		BillOfMaterialElement[] retNodes;
//		// This method recurses, traversing towards the root in order
//		// size the array. On the way back, it fills in the nodes,
//		// starting from the root and working back to the original node.
//
//		/* 
//		 * Check for null, in case someone passed in a null node, or
//		 * they passed in an element that isn't rooted at root.
//		 */
//		if(aNode == null)
//		{
//			if(depth == 0)
//			{
//				return null;
//			}
//			else
//			{
//				retNodes = new BillOfMaterialElement[depth];
//			}
//		}
//		else
//		{
//			depth++;
//			if(aNode == root)
//			{
//				retNodes = new Material[depth];
//			} 
//			else
//			{
//				retNodes = getPathToRoot(new Material(aNode.getParent()), depth);
//			} 
//			retNodes[retNodes.length - depth] = aNode;
//		}
//		return retNodes;
//	}

    /**
     * Notifies all listeners that have registered interest for notification on this
     * event type. The event instance is lazily created using the parameters passed
     * into the fire method.
     */
    private void fireTreeStructureChanged(Object source, TreePath path) {
        // Guaranteed to return a non-null array
        Object[] listeners = listenerList.getListenerList();
        // Lazily create the event:
        TreeModelEvent e = new TreeModelEvent(source, path);
        // Process the listeners last to first, notifying
        // those that are interested in this event
        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            ((TreeModelListener) listeners[i + 1]).treeStructureChanged(e);
        }
    }

    /**
     * Notifies all listeners that have registered interest for notification on this
     * event type. The event instance is lazily created using the parameters passed
     * into the fire method.
     *
     * @param source       the node where the tree model has changed
     * @param path         the path to the root node
     * @param childIndices the indices of the affected elements
     * @param children     the affected elements
     * @see EventListenerList
     */
    protected void fireTreeStructureChanged(Object source, Object[] path, int[] childIndices, Object[] children) {
        // Guaranteed to return a non-null array
        Object[] listeners = listenerList.getListenerList();
        TreeModelEvent e = new TreeModelEvent(source, path, childIndices, children);
        // Process the listeners last to first, notifying
        // those that are interested in this event
        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            ((TreeModelListener) listeners[i + 1]).treeStructureChanged(e);
        }
    }

    /**
     * This model isn't producing any events at present, so this method is empty.
     *
     * @param treeModelListener The listener to register
     */
    @Override
    public synchronized void addTreeModelListener(TreeModelListener treeModelListener) {
        listenerList.add(TreeModelListener.class, treeModelListener);
    }

    /**
     * This model isn't producing any events at present, so this method is empty.
     *
     * @param treeModelListener The listener to be removed from the internal listener list
     */
    @Override
    public synchronized void removeTreeModelListener(TreeModelListener treeModelListener) {
        listenerList.remove(TreeModelListener.class, treeModelListener);
    }

    /**
     * Returns an array of all the tree model listeners registered on this model.
     *
     * @return all of this model's <code>TreeModelListener</code>s or an empty array
     * if no tree model listeners are currently registered
     * @see #addTreeModelListener
     * @see #removeTreeModelListener
     */
    public TreeModelListener[] getTreeModelListeners() {
        return listenerList.getListeners(TreeModelListener.class);
    }

}
