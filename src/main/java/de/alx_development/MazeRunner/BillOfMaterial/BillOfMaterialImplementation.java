package de.alx_development.MazeRunner.BillOfMaterial;

/*-
 * #%L
 * MazeRunner configuration management
 * %%
 * Copyright (C) 2019 ALX-Development
 * %%
 * This file is part of MazeRunner.
 *
 * MazeRunner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MazeRunner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil des MazeRunner Projekt.
 *
 * MazeRunner ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * MazeRunner wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import javax.swing.event.EventListenerList;
import java.util.ArrayList;

/**
 * class to handle a bill of materials
 *
 * @author Alexander Thiel
 */
abstract class BillOfMaterialImplementation implements BillOfMaterial {

    private final ArrayList<BillOfMaterialElement> elements = new ArrayList<>();
    /**
     * List of listeners
     */
    protected EventListenerList listenerList = new EventListenerList();
    private boolean loaded = false;

    /**
     * Default constructor without any additional parameter.
     */
    protected BillOfMaterialImplementation() {
        super();
    }

    @Override
    public void addPosition(BillOfMaterialElement position) {
        elements.add(position);
        fireListStructureChanged(this);
    }

    @Override
    public void removePosition(int index) {
        elements.remove(index);
        fireListStructureChanged(this);
    }

    @Override
    public void reload() {
        elements.clear();
        loaded = true;
        MaterialFactory.getInstance().getLoader().load(this);
        fireListStructureChanged(this);
    }

    @Override
    public int getSize() {
        if (!loaded) {
            reload();
        }
        return elements.size();
    }

    @Override
    public BillOfMaterialElement getElementAt(int index) {
        return elements.get(index);
    }

    protected void fireListStructureChanged(BillOfMaterial source) {
        // Guaranteed to return a non-null array
        Object[] listeners = listenerList.getListenerList();
        BillOfMaterialChangeEvent e = new BillOfMaterialChangeEvent(source, BillOfMaterialChangeEvent.CONTENTS_CHANGED, 0, getSize());
        // Process the listeners last to first, notifying
        // those that are interested in this event
        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            ((BillOfMaterialChangeListener) listeners[i + 1]).contentsChanged(e);
        }
    }

    public synchronized void addBillOfMaterialChangeListener(BillOfMaterialChangeListener l) {
        listenerList.add(BillOfMaterialChangeListener.class, l);
    }

    public synchronized void removeBillOfMaterialChangeListener(BillOfMaterialChangeListener l) {
        listenerList.remove(BillOfMaterialChangeListener.class, l);
    }
}
