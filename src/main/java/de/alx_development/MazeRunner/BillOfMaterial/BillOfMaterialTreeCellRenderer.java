package de.alx_development.MazeRunner.BillOfMaterial;

/*-
 * #%L
 * MazeRunner configuration management
 * %%
 * Copyright (C) 2019 ALX-Development
 * %%
 * This file is part of MazeRunner.
 *
 * MazeRunner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MazeRunner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil des MazeRunner Projekt.
 *
 * MazeRunner ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * MazeRunner wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import javax.swing.*;
import javax.swing.tree.DefaultTreeCellRenderer;
import java.awt.*;

/**
 * @author Alexander Thiel
 */
public class BillOfMaterialTreeCellRenderer extends DefaultTreeCellRenderer {

    private static final long serialVersionUID = -7578146854723442690L;

    /**
     * Importing the icons which represent whether the
     * <code>BillOfMaterialElement</code> is an assembly or not.
     */
    private static final ImageIcon icon_assembly = new ImageIcon(
            BillOfMaterialTreeCellRenderer.class.getResource("BillOfMaterial.Assembly.ICON.16x16.png"));
    private static final ImageIcon icon_component = new ImageIcon(
            BillOfMaterialTreeCellRenderer.class.getResource("BillOfMaterial.Component.ICON.16x16.png"));

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded,
                                                  boolean leaf, int row, boolean hasFocus) {

        super.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);

        // Wert aus der Liste wird als Person interpretiert (gecastet)
        BillOfMaterialElement bomElement = (BillOfMaterialElement) value;

        // Rückgabe der toString-Methode wird als Text gesetzt
        this.setText(bomElement.toString());

        if (bomElement.isAssembly()) {
            this.setIcon(icon_assembly);
        } else {
            this.setIcon(icon_component);
        }

        // Das Label wird zurückgegeben und nun angezeigt
        return this;
    }

}
