package de.alx_development.MazeRunner.BillOfMaterial;

/*-
 * #%L
 * MazeRunner configuration management
 * %%
 * Copyright (C) 2019 ALX-Development
 * %%
 * This file is part of MazeRunner.
 *
 * MazeRunner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MazeRunner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil des MazeRunner Projekt.
 *
 * MazeRunner ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * MazeRunner wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

/**
 * @author Alexander Thiel
 */
public abstract class Material extends BillOfMaterialImplementation {

    private final String strMaterialNumber;
    private String strMaterialName;
    private String strMaterialInfo;

    /**
     * Constructor
     *
     * @param name   - name of the material
     * @param number - number of the material
     */
    public Material(String name, String number) {
        this.strMaterialName = name;
        this.strMaterialNumber = number;
    }

    /**
     * gets the name of the material
     *
     * @return name of Material
     */
    public String getMaterialName() {
        return this.strMaterialName;
    }

    /**
     * sets the name of the material
     *
     * @param name - name of material
     */
    public void setMaterialName(String name) {
        strMaterialName = name;
    }

    /**
     * gets the number of the material
     *
     * @return number of the material
     */
    public String getMaterialNumber() {
        return this.strMaterialNumber;
    }

    /**
     * gets the addtional info of the material
     *
     * @return additional info of Material
     */
    public String getMaterialInfo() {
        return this.strMaterialInfo;
    }

    /**
     * sets the additional info of the material
     *
     * @param info - additional info of material
     */
    public void setMaterialInfo(String info) {
        strMaterialInfo = info;
    }

    /**
     * returns <code>true</code> if the material is an assembly and carries a bill
     * of material
     *
     * @return - true, if material is an assembly
     */
    public boolean isAssembly() {
        return this.getSize() > 0;
    }

    @Override
    public String toString() {
        return getMaterialNumber();
    }

}
