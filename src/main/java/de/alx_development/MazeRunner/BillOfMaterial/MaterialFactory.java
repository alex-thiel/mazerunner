package de.alx_development.MazeRunner.BillOfMaterial;

/*-
 * #%L
 * MazeRunner configuration management
 * %%
 * Copyright (C) 2019 ALX-Development
 * %%
 * This file is part of MazeRunner.
 *
 * MazeRunner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MazeRunner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil des MazeRunner Projekt.
 *
 * MazeRunner ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * MazeRunner wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import java.util.HashMap;

/**
 * @author Alexander Thiel
 */
public class MaterialFactory {

    private static MaterialFactory instance;

    private final HashMap<String, Materialx> materials = new HashMap<>();
    private BillOfMaterialLoader loader;

    /**
     *
     */
    protected MaterialFactory() {
        //Application.logger.log(Level.FINE, "Initializing MaterialFactory instance");
    }

    /**
     * Use this function to get a handle to the local <code>MaterialFactory</code>
     * object which handles all tasks for the running application.
     *
     * @return A reference to the <code>MaterialFactory</code>
     */
    public static MaterialFactory getInstance() {
        if (instance == null)
            instance = new MaterialFactory();

        return instance;
    }

    /**
     * This function returns a material representation for the given
     * material id number.
     * If the material is not available, it will be created.
     *
     * @param number - the material id number
     * @return Material representation for the given material id
     */
    public Material getMaterial(String number) {
        if (!materials.containsKey(number)) {
            createMaterial(number, null);
        }

        return materials.get(number);
    }

    /**
     * This function returns true, if a material with the given
     * id number is existing in the internal material list.
     *
     * @param number - the id of the requested material
     * @return true, if the material id is available
     */
    public boolean containsMaterial(String number) {
        return materials.containsKey(number);
    }

    /**
     * Method which triggers the creation and adding a new material to the
     * material list.
     *
     * @param number - the material id number
     * @param name   - the name or description of the material
     */
    public void createMaterial(String number, String name) {
        Materialx material = new Materialx(name, number);
        materials.put(number, material);
    }

    /**
     * This function returns the actual active loader for material and
     * bill of materials
     *
     * @return reference to the active loader
     */
    public BillOfMaterialLoader getLoader() {
        return loader;
    }

    /**
     * Use this to set the loader implementation of <code>BillOfMaterialLoader</code>
     * which is used to load the material attributes and bill of material elements.
     *
     * @param loader - the loader to use
     */
    public void setLoader(BillOfMaterialLoader loader) {
        this.loader = loader;

        // Changing the loader reference of already listed material
        for (String s : materials.keySet()) {
            materials.get(s).reload();
        }
    }

    private static class Materialx extends Material {

        public Materialx(String name, String number) {
            super(name, number);
        }
    }
}
