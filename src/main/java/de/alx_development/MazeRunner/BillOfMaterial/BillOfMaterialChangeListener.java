package de.alx_development.MazeRunner.BillOfMaterial;

/*-
 * #%L
 * MazeRunner configuration management
 * %%
 * Copyright (C) 2019 ALX-Development
 * %%
 * This file is part of MazeRunner.
 *
 * MazeRunner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MazeRunner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil des MazeRunner Projekt.
 *
 * MazeRunner ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * MazeRunner wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import java.util.EventListener;

/**
 * A <code>BillOfMaterialChangeListener</code> can register with a {@link BillOfMaterial} and
 * receive notification of updates to the structure.
 *
 * @author Alexander Thiel
 */
public interface BillOfMaterialChangeListener extends EventListener {

    /**
     * Notifies the listener that the contents of the list have changed
     * in some way.  This method will be called if the change cannot be
     * notified via the {@link #intervalAdded(BillOfMaterialChangeEvent)} or the
     * {@link #intervalRemoved(BillOfMaterialChangeEvent)} methods.
     *
     * @param event the event.
     */
    void contentsChanged(BillOfMaterialChangeEvent event);

    /**
     * Notifies the listener that one or more items have been added to the
     * list.  The <code>event</code> argument can supply the indices for the
     * range of items added.
     *
     * @param event the event.
     */
    void intervalAdded(BillOfMaterialChangeEvent event);

    /**
     * Notifies the listener that one or more items have been removed from
     * the list.  The <code>event</code> argument can supply the indices for
     * the range of items removed.
     *
     * @param event the event.
     */
    void intervalRemoved(BillOfMaterialChangeEvent event);
}
