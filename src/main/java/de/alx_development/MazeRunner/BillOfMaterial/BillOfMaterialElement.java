package de.alx_development.MazeRunner.BillOfMaterial;

/*-
 * #%L
 * MazeRunner configuration management
 * %%
 * Copyright (C) 2019 ALX-Development
 * %%
 * This file is part of MazeRunner.
 *
 * MazeRunner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MazeRunner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil des MazeRunner Projekt.
 *
 * MazeRunner ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * MazeRunner wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import javax.swing.event.EventListenerList;
import java.util.HashMap;
import java.util.Set;

/**
 * @author Alexander Thiel
 */
public class BillOfMaterialElement {

    private static final String ATTRIBUTE_MATERIAL = "org.alx.BillOfMaterial.BillOfMaterialElement.Material.ID";
    private static final String ATTRIBUTE_AMOUNT = "org.alx.BillOfMaterial.BillOfMaterialElement.Amount.ID";

    private final HashMap<String, Object> attributes = new HashMap<>();

    /**
     * List of listeners
     */
    protected EventListenerList listeners = new EventListenerList();

    public BillOfMaterialElement(Double amount, String materialnumber) {
        this(amount, MaterialFactory.getInstance().getMaterial(materialnumber));
    }

    public BillOfMaterialElement(Double amount, Material material) {
        setMaterial(material);
        setAmount(amount);
    }

    public Double getAmount() {
        return (Double) getAttribute(BillOfMaterialElement.ATTRIBUTE_AMOUNT);
    }

    public void setAmount(Double amount) {
        putAttribute(BillOfMaterialElement.ATTRIBUTE_AMOUNT, amount);
    }

    public Material getMaterial() {
        return (Material) getAttribute(BillOfMaterialElement.ATTRIBUTE_MATERIAL);
    }

    public void setMaterial(Material material) {
        putAttribute(BillOfMaterialElement.ATTRIBUTE_MATERIAL, material);
    }

    public boolean isAssembly() {
        return getMaterial().isAssembly();
    }

    /**
     * Returns true if this attribute requested is available.
     *
     * @param key - The attributes key
     * @return availability of the attribute
     */
    public boolean containsAttributeKey(String key) {
        return attributes.containsKey(key);
    }

    /**
     * Returns true if the attributes contains one or more keys to the specified
     * value.
     *
     * @param value - The requested attributes value
     * @return availability of attributes with the given value
     */
    public boolean containsAttributeValue(Object value) {
        return attributes.containsValue(value);
    }

    /**
     * Returns the value to which the specified key is mapped, or null if the
     * attributes contains no mapping for the key.
     *
     * @param key - the requested attribute
     * @return the attributes value
     */
    public Object getAttribute(String key) {
        return attributes.get(key);
    }

    /**
     * Associates the specified value with the specified key in the attribute list.
     *
     * @param key   - the key to identify the value
     * @param value - the value to store as attribute
     */
    public void putAttribute(String key, Object value) {

        // Ensure not to remove amount or material
        if ((value == null) && (key.equals(ATTRIBUTE_AMOUNT) || key.equals(ATTRIBUTE_MATERIAL))) {
            throw new NullPointerException("null value is not allowed for material or amount attributes");
        }

        // Determine which event type to fire
        int type;
        if (containsAttributeKey(key)) {
            type = BillOfMaterialElementChangeEvent.ATTRIBUTES_CHANGED;
        } else {
            type = BillOfMaterialElementChangeEvent.ATTRIBUTES_ADDED;
        }

        attributes.put(key, value);
        fireAttributesChanged(key, type);
    }

    /**
     * Removes the mapping for the specified key from attributes if present.
     *
     * @param key - the key for the attribute to remove
     */
    public void removeAttribute(String key) {
        if (containsAttributeKey(key)) {
            removeAttribute(key, getAttribute(key));
        }
    }

    /**
     * Removes the entry for the specified key only if it is currently mapped to the
     * specified value.
     *
     * @param key   - key with which the specified value is associated
     * @param value - value expected to be associated with the specified key
     * @return true if the value was removed
     */
    public boolean removeAttribute(String key, Object value) throws UnsupportedOperationException {

        // Ensure not to remove amount or material
        if (key.equals(ATTRIBUTE_AMOUNT) || key.equals(ATTRIBUTE_MATERIAL)) {
            throw new UnsupportedOperationException("Removing of material or amount attributes is not allowed");
        }

        if (attributes.remove(key, value)) {
            fireAttributesChanged(key, BillOfMaterialElementChangeEvent.ATTRIBUTES_REMOVED);
            return true;
        }
        return false;
    }

    /**
     * Returns the number of key-value mappings in attributes.
     *
     * @return the number of key-value mappings in attributes
     */
    public int getAttributeSize() {
        return attributes.size();
    }

    /**
     * Returns a Set view of the keys contained in this map.
     *
     * @return a set view of the keys contained in the attributes
     */
    public Set<String> getAttributesKeySet() {
        return attributes.keySet();
    }

    /**
     * This method is used to fire change event notifications if attributes have
     * changed. All registered listeners will be notified about the change.
     *
     * @param key - the key of the changed attribute
     */
    protected void fireAttributesChanged(String key) {
        fireAttributesChanged(key, BillOfMaterialElementChangeEvent.ATTRIBUTES_CHANGED);
    }

    /**
     * This method is used to fire change event notifications if attributes have
     * changed. All registered listeners will be notified about the change.
     *
     * @param key  - the key of the changed attribute
     * @param type - the type of the event to fire
     */
    protected void fireAttributesChanged(String key, int type) {
        // Guaranteed to return a non-null array
        Object[] listenerList = listeners.getListenerList();
        BillOfMaterialElementChangeEvent e = new BillOfMaterialElementChangeEvent(this, type, key);
        // Process the listeners last to first, notifying
        // those that are interested in this event
        for (int i = listenerList.length - 2; i >= 0; i -= 2) {
            ((BillOfMaterialElementChangeListener) listenerList[i + 1]).attributesChanged(e);
        }
    }

    public synchronized void addBillOfMaterialElementChangeListener(BillOfMaterialElementChangeListener l) {
        listeners.add(BillOfMaterialElementChangeListener.class, l);
    }

    public synchronized void removeBillOfMaterialElementChangeListener(BillOfMaterialElementChangeListener l) {
        listeners.remove(BillOfMaterialElementChangeListener.class, l);
    }

    @Override
    public String toString() {
        return getMaterial().getMaterialNumber() + " [" + getAmount() + " " + getAttribute("ME") + "]" + " "
                + getAttribute("Bezeichnung");
    }

}
