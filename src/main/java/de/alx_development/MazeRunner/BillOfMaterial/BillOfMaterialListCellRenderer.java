package de.alx_development.MazeRunner.BillOfMaterial;

/*-
 * #%L
 * MazeRunner configuration management
 * %%
 * Copyright (C) 2019 ALX-Development
 * %%
 * This file is part of MazeRunner.
 *
 * MazeRunner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MazeRunner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil des MazeRunner Projekt.
 *
 * MazeRunner ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * MazeRunner wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import javax.swing.*;
import java.awt.*;

/**
 * @author Alexander Thiel
 */
public class BillOfMaterialListCellRenderer extends JLabel
        implements ListCellRenderer<BillOfMaterialElement>, BillOfMaterialElementChangeListener {

    private static final long serialVersionUID = -2664537297283033543L;

    /**
     * Importing the icons which represent whether the
     * <code>BillOfMaterialElement</code> is an assembly or not.
     */
    private static final ImageIcon icon_assembly = new ImageIcon(
            BillOfMaterialListCellRenderer.class.getResource("BillOfMaterial.Assembly.ICON.16x16.png"));
    private static final ImageIcon icon_component = new ImageIcon(
            BillOfMaterialListCellRenderer.class.getResource("BillOfMaterial.Component.ICON.16x16.png"));

    @Override
    public Component getListCellRendererComponent(JList<? extends BillOfMaterialElement> list,
                                                  BillOfMaterialElement value, int index, boolean isSelected, boolean cellHasFocus) {

        setOpaque(true); // Muss aufgerufen werden ansonsten hat this.setBackground keine Wirkung

        // Wert aus der Liste wird als BillOfMaterialElement interpretiert (gecastet)

        // Der anzuzeigende Text wird vereinfacht der to
        setText(value.getMaterial().getMaterialNumber() + " [" + value.getAmount() + " "
                + value.getAttribute("ME") + "]" + " " + value.getAttribute("Bezeichnung"));

        // Setting the text to plain style instead of the default bold style
        this.setFont(this.getFont().deriveFont(Font.PLAIN));

        // Element aus der Liste ist markiert
        if (isSelected) {
            // Schriftfarbe
            // UIManager.getColor("List.selectionForeground") gibt die
            // Standard Schriftfarbe für ein markiertes Listen Element zurück
            setForeground(UIManager.getColor("List.selectionForeground"));
            // Hintergrund
            // UIManager.getColor("List.selectionBackground") gibt die
            // Standard Hintergrundfarbe für ein markiertes Listen Element zurück
            setBackground(UIManager.getColor("List.selectionBackground"));
        }
        // Element aus der Liste ist nicht markiert
        else {
            // Schriftfarbe
            setForeground(UIManager.getColor("List.foreground"));
            // Hintergrund
            this.setBackground(UIManager.getColor("List.background"));
        }

        // Setting the icon according the element type
        if (value.isAssembly()) {
            setIcon(icon_assembly);
        } else {
            setIcon(icon_component);
        }

        /*
         * To be notified in case of attribute changes which should cause a repaint of
         * the cell renderer it is necessary to add this as listener
         */
        value.addBillOfMaterialElementChangeListener(this);

        // Das Label wird zurückgegeben und nun angezeigt
        return this;
    }

    /**
     * This function is called if an attribute of the
     * <code>BillOfMaterialElement</code> which is represented by this renderer has
     * been changed.
     *
     * @param event - reference to the event object
     */
    @Override
    public void attributesChanged(BillOfMaterialElementChangeEvent event) {
        repaint(500);
    }
}
