package de.alx_development.MazeRunner;

/*-
 * #%L
 * MazeRunner configuration management
 * %%
 * Copyright (C) 2019 ALX-Development
 * %%
 * This file is part of MazeRunner.
 *
 * MazeRunner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MazeRunner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil des MazeRunner Projekt.
 *
 * MazeRunner ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * MazeRunner wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import de.alx_development.application.Bootstrapper;
import de.alx_development.application.JApplicationFrame;
import de.alx_development.MazeRunner.Configuration.Configuration;
import de.alx_development.MazeRunner.Product.DefaultProjectLoader;
import de.alx_development.MazeRunner.Product.Project;
import de.alx_development.MazeRunner.Product.ProjectView;
import de.alx_development.MazeRunner.UI.ConfigurationEditorPanel;

import javax.swing.*;
import java.awt.*;
import java.util.Objects;

/**
 * The main class of the MazeRunner application. Run this one if
 * you'd like to use the graphical frontend to handle complex
 * product configurations in an easy way.
 *
 * @author Alexander Thiel
 */
public class MazeRunner extends JApplicationFrame {
    /**
     *
     */
    private static final long serialVersionUID = 6551561462173856818L;

    public MazeRunner() {
        /*
         * Setting up the application environment like name, version and application
         * icon.
         */
        super();
        setTitle("MazeRunner");
        setVersion("0.0.1");
        setApplicationIcon(new ImageIcon(Objects.requireNonNull(MazeRunner.class.getResource("MazeRunner.ICON.32x32.png"))));

        /*
         * GUI structure elements
         */
        JSplitPane splitPaneVert1;

        // Component which displays the project's product structure
        ProjectView projectview = new ProjectView();
        Project project = new Project();
        /*
         * Loading the project content using the dummy loader
         * TODO: Must be implemented
         */
        new DefaultProjectLoader().load(project, "Dummy Project");
        projectview.setProject(project);
        getContentPane().add(projectview, BorderLayout.CENTER);

        // Configuration environment
        ConfigurationEditorPanel configPanel = new ConfigurationEditorPanel(new Configuration("Test"));

        splitPaneVert1 = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, projectview, configPanel);
        splitPaneVert1.setOneTouchExpandable(true);
        splitPaneVert1.setResizeWeight(1.0);

        getContentPane().add(splitPaneVert1, BorderLayout.CENTER);
    }

    /**
     * @param args The command line arguments passed to the application
     */
    public static void main(String[] args) {
        Bootstrapper.load(MazeRunner.class);
    }
}
