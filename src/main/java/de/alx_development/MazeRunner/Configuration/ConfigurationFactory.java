package de.alx_development.MazeRunner.Configuration;

/*-
 * #%L
 * MazeRunner configuration management
 * %%
 * Copyright (C) 2019 ALX-Development
 * %%
 * This file is part of MazeRunner.
 *
 * MazeRunner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MazeRunner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil des MazeRunner Projekt.
 *
 * MazeRunner ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * MazeRunner wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import java.util.HashMap;
import java.util.Properties;

/**
 * @author Alexander Thiel
 */
public class ConfigurationFactory {
    private static ConfigurationFactory instance;
    private final Properties defaultproperties = new Properties();
    private final HashMap<String, Configuration> configurations = new HashMap<>();
    private ConfigurationLoader loader;

    /**
     * Use this function to get a handle to the local <code>ConfigurationFactory</code>
     * object which handles all tasks for the running application.
     *
     * @return A reference to the <code>ConfigurationFactory</code>
     */
    public static ConfigurationFactory getInstance() {
        if (instance == null)
            instance = new ConfigurationFactory();

        return instance;
    }

    /**
     * This function provides direct access to the <code>ConfigurationLoader</code> instance
     * used to load and store the configuration objects into the persistence system
     *
     * @return The actual active ConfigurationLoader
     */
    public ConfigurationLoader getConfigurationLoader() {
        return this.loader;
    }

    /**
     * Use this function to set the ConfigurationLoader which should be used for further
     * load and store activities.
     *
     * @param loader The ConfigurationLoader which should be used to cooperate with the persistence system
     */
    public synchronized void setConfigurationLoader(ConfigurationLoader loader) {
        this.loader = loader;
    }


    public String getDefaultProperty(String key) {
        return this.defaultproperties.getProperty(key);
    }

    public void setDefaultProperty(String key, String value) {
        this.defaultproperties.setProperty(key, value);
    }

    public Configuration getConfiguration(String id) {
        if (!configurations.containsKey(id)) {
            //TODO: Properties merge has to be implemented to make it possible
            //      to pass individual properties to the load process
            this.addConfiguration(id, getConfigurationLoader().load(defaultproperties));
        }
        return configurations.get(id);
    }

    public void addConfiguration(String id, Configuration config) {
        configurations.put(id, config);
    }
}
