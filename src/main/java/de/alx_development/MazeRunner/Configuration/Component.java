package de.alx_development.MazeRunner.Configuration;

/*-
 * #%L
 * MazeRunner configuration management
 * %%
 * Copyright (C) 2019 ALX-Development
 * %%
 * This file is part of MazeRunner.
 *
 * MazeRunner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MazeRunner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil des MazeRunner Projekt.
 *
 * MazeRunner ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * MazeRunner wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import de.alx_development.application.Application;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

/**
 * @author Alexander Thiel
 */
public class Component {
    private final List<Variant> variants = new ArrayList<>();
    private String name;

    /**
     * Use this constructor to initialize a new component object.
     * As parameter the name of the component is expected.
     *
     * @param name The name of the component
     */
    public Component(String name) {
        super();
        setName(name);
    }

    /**
     * Use this function to get the name of the component given in the
     * constructor.
     *
     * @return Name of the component
     */
    public String getName() {
        return name;
    }

    /**
     * Use this function to change the name of the component. An empty name or a <code>null</code>
     * is not accepted as parameter. In this case the given parameter is ignored and the former
     * assigned name is still active.
     *
     * @param name The new name for the component (Must not be empty or null)
     * @return true, if the name has been changed successfully
     */
    public boolean setName(String name) {
        if (!(name == null || name.isEmpty())) {
            this.name = name;
        }
        return (this.name.equals(name));
    }

    /**
     * Use this function to get a reference to the active variant
     * of this component. If there is no activated variant the function
     * returns a null value.
     *
     * @return The active variant of this component
     */
    public Variant getActiveVariant() {
        for (Variant variant : variants) {
            if (variant.isActive()) {
                return variant;
            }
        }
        return null;
    }

    /**
     * Use this function to set a specific variant as active. To do this,
     * the variant, which should be activated must be given as reference.
     * If the given variant is not member of this component, nothing will
     * happen. To add an active variant to this component, the variant must
     * be added first to the component.
     *
     * @param variant The variant to activate
     * @see #addVariant(Variant)
     */
    public void setActiveVariant(Variant variant) {
        // Iterating over all known Variants and setting the referenced
        // one as active
        for (Variant value : variants) {
            value.setActive(variant);
        }
    }

    /**
     * This function returns the number of variants assigned to this
     * component. Use this in conjunction with the <code>getVariantAt(index)</code>
     * function to iterate over the available variant objects.
     *
     * @return Number of variants assigned to this component
     */
    public int getSize() {
        return variants.size();
    }

    /**
     * Removes the first occurrence of the specified variant from this component,
     * if it is present (optional operation). If this component does not contain the variant,
     * it is unchanged.
     * Returns true if this list contained the specified variant (or equivalently, if this list changed
     * as a result of the call).
     *
     * @param variant The variant to be removed
     * @return true, if the variant has been removed successfully
     */
    public boolean removeVariant(Variant variant) {
        return variants.remove(variant);
    }

    /**
     * Removes the variant at the specified position in this list (optional
     * operation). Shifts any subsequent elements to the left (subtracts one
     * from their indices). Returns the element that was removed from the
     * list.
     *
     * @param index The index to remove
     * @return The variant which has been removed
     */
    public Variant removeVariant(int index) {
        return variants.remove(index);
    }

    /**
     * Removes all of the variants from this component.
     * The component will be empty after this call returns.
     */
    public void clearVariants() {
        variants.clear();
    }

    /**
     * Use this function to register a new variant at this component. As the variant list
     * in the component is not ordered by any specific rule the newly added variant will be
     * appended to the end of the variant list.
     *
     * @param variant The variant to add to this component.
     */
    public void addVariant(Variant variant) {
        variants.add(variant);
    }

    /**
     * This function is collecting all commands from the assigned active
     * variants and returning a complete list of all activated commands.
     * If there are no active commands an empty list object is returned.
     *
     * @return List of all commands from all active variants
     */
    public List<String> getCommands() {
        List<String> commandlist = new ArrayList<>();

        /*
         * Iterating over all known Variants and collecting all
         * commands to get a complete command structure for the
         * component
         */
        for (Variant variant : variants) {
            if (variant.isActive()) {
                commandlist.addAll(variant.getCommands());
            }
        }

        return commandlist;
    }

    /**
     * This function returns a complete list of all variable assigned to the
     * active variants of this component.
     *
     * @return A Map object containing all key value pairs of the the variables
     */
    public Map<String, String> getVariables() {
        Map<String, String> variablelist = new HashMap<>();

        /*
         * Iterating over all known Variants and collecting all
         * commands to get a complete command structure for the
         * component
         */
        for (Variant variant : variants) {
            if (variant.isActive()) {
                for (String key : variant.getVariables()) {
                    /*
                     *  Adding the variable to the complete collection if key does not
                     *  already exist.
                     */

                    if (!variablelist.containsKey(key)) {
                        variablelist.put(key, variant.getVariableValue(key));
                    } else {
                        Application.logger.log(Level.SEVERE, "Variable [" + key + "] already defined");
                    }
                }
            }
        }

        return variablelist;
    }

    /**
     * This function simply returns true, if the number of assigned
     * variants is zero.
     *
     * @return true, if there are no variants available
     */
    public boolean isEmpty() {
        return variants.isEmpty();
    }

    /**
     * This function returns true, if the given <code>Variant</code> is
     * assigned to this component. May be used, to proof if a variant is member
     * of this component.
     *
     * @param variant The variant to proof
     * @return true, if variant is member of this component
     */
    public boolean contains(Variant variant) {
        return variants.contains(variant);
    }

    /**
     * This function returns the variant at the given index. Use the
     * <code>getSize()</code> function to determine how many variants are available.
     *
     * @param index The index of the variant
     * @return The variant at the given index
     */
    public Variant getVariantAt(int index) {
        return variants.get(index);
    }

    /**
     * Returns the index of the first occurrence of the specified variant in
     * this list, or -1 if this list does not contain the element.More formally,
     * returns the lowest index i,or -1 if there is no such index.
     *
     * @param variant The variant to look for
     * @return The index of the variant assigned to this component
     */
    public int indexOf(Variant variant) {
        return variants.indexOf(variant);
    }
}
