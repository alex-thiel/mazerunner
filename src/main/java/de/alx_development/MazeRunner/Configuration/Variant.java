package de.alx_development.MazeRunner.Configuration;

/*-
 * #%L
 * MazeRunner configuration management
 * %%
 * Copyright (C) 2019 ALX-Development
 * %%
 * This file is part of MazeRunner.
 *
 * MazeRunner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MazeRunner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil des MazeRunner Projekt.
 *
 * MazeRunner ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * MazeRunner wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import java.io.Serializable;
import java.util.*;

/**
 * @author Alexander Thiel
 */
public class Variant implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -8512241487093167946L;
    private final List<String> commands = new ArrayList<>();
    private final Map<String, String> variables = new HashMap<>();
    private String name;
    private boolean active = false;

    /**
     * Constructor to instantiate a new variant object which contains a specific
     * appearance of a component.
     *
     * @param name The name for the variant
     */
    public Variant(String name) {
        super();
        setName(name);
    }

    /**
     * Use this function to get the name of the variant.
     *
     * @return The name of the variant
     */
    public String getName() {
        return name;
    }


    /**
     * Use this function to change the name of the component. An empty name or a <code>null</code>
     * is not accepted as parameter. In this case the given parameter is ignored and the former
     * assigned name is still active.
     *
     * @param name The new name for the component (Must not be empty or null)
     * @return true, if the name has been changed successfully
     */
    public boolean setName(String name) {
        if (!(name == null || name.isEmpty())) {
            this.name = name;
        }
        return (this.name.equals(name));
    }

    /**
     * This function returns a list of all commands defined in this variant.
     * The list is a copy of the internal list. So changes to the reference returned
     * by this function don't have any effect to the commands in the variant. To
     * change the content use the special commands for adding and removing commands.
     *
     * @return A list of the commands of the variant
     * @see #addCommand(String)
     * @see #removeCommand(String)
     */
    public List<String> getCommands() {
        return new ArrayList<>(this.commands);
    }

    /**
     * Use this function to add a command to the internal command structure.
     *
     * @param command The command which should be added
     */
    public void addCommand(String command) {
        this.commands.add(command);
    }

    /**
     * Removes the first occurrence of the specified command from this list,
     * if it is present (optional operation). If this list does not contain
     * the command, it is unchanged.
     * Returns true if this list contained the specified element (or equivalently,
     * if this list changed as a result of the call).
     *
     * @param command The command to remove
     * @return true, if the command has been found and was removed
     */
    public boolean removeCommand(String command) {
        return this.commands.remove(command);
    }

    /**
     * Use this function to add a new variable to the variant.
     *
     * @param key   The key name of the variable
     * @param value The variable value
     */
    public void addVariable(String key, String value) {
        this.variables.put(key, value);
    }

    /**
     * Removes the mapping for a key from this map if it is present (optional operation).
     * More formally, if this map contains a mapping from key k to value v, that mapping is removed.
     * (The map can contain at most one such mapping.)
     *
     * @param key The key for the variable to remove
     * @return Returns the value to which this map previously associated the key, or null if the map contained no mapping for the key.
     */
    public String removeVariable(String key) {
        return variables.remove(key);
    }

    /**
     * Returns a Set view of the keys contained in this variables list. The set is backed by the map,
     * so changes to the map are reflected in the set, and vice-versa. If the map is modified
     * while an iteration over the set is in progress (except through the iterator's own remove
     * operation), the results of the iteration are undefined. The set supports element removal,
     * which removes the corresponding mapping from the map, via the Iterator.remove,
     * Set.remove, removeAll, retainAll, and clear operations. It does not support the add or
     * addAll operations.
     *
     * @return a set view of the variable keys contained in this variant
     */
    public Set<String> getVariables() {
        return this.variables.keySet();
    }

    /**
     * Returns the value to which the specified key is mapped,
     * or null if this variant contains no variable for the key.
     *
     * @param name The key name of the variable
     * @return The variable value as string
     */
    public String getVariableValue(String name) {
        return this.variables.get(name);
    }

    /**
     * Returns true, if the variant is set as active in the concrete
     * configuration. In this case the commands and variables propagated
     * to the component.
     *
     * @return true, if the variant is declared as active in the component
     */
    public boolean isActive() {
        return active;
    }

    /**
     * This function is package internally used to the the <code>active</code> bit if the
     * variant is activated in the component.
     * To activate this variant, a reference to itself must be passed as parameter.
     *
     * @param variant The variant which should be activated
     */
    protected void setActive(Variant variant) {
        this.active = (this == variant);
    }
}
