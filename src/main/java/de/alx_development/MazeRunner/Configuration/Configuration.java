package de.alx_development.MazeRunner.Configuration;

/*-
 * #%L
 * MazeRunner configuration management
 * %%
 * Copyright (C) 2019 ALX-Development
 * %%
 * This file is part of MazeRunner.
 *
 * MazeRunner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MazeRunner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil des MazeRunner Projekt.
 *
 * MazeRunner ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * MazeRunner wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import com.google.gson.Gson;
import de.alx_development.MazeRunner.Generator.Generator;
import de.alx_development.MazeRunner.Generator.GeneratorDescription;
import de.alx_development.MazeRunner.Generator.GeneratorFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Alexander Thiel
 */
public class Configuration {
    /**
     * A <code>Configuration</code> contains a list of <code>Components</code> which
     * themselves contains different variants specifying the configuration details.
     */
    private final List<Component> components = new ArrayList<>();
    /**
     * To build some output based upon the configuration it is neccessary to use
     * generators. Generators and their parameters are managed inside the
     * configuration itself.
     */
    private final List<GeneratorDescription> generators = new ArrayList<>();
    /**
     * The name is a naming and description for the configuration for human readable
     * declaration.
     */
    private String name;

    /**
     * Default constructor expecting the name of the configuration. The name can be
     * changed later on using the function <code>setName()</code>.
     *
     * @param name - The name of the configuration
     * @see #setName(String)
     */
    public Configuration(String name) {
        super();
        this.setName(name);
    }

    /**
     * This static function may be used to generate a JSON representation of the
     * configuration object.
     *
     * @param config - The configuration to convert into JSON
     * @return A string representing the object structure in JSON
     */
    public static String toJson(Configuration config) {
        return new Gson().toJson(config);
    }

    /**
     * Function to get a configuration object structure from a JSON string
     * description.
     *
     * @param json - The string containing the JSON representation
     * @return The configuration built using the JSON structure
     */
    public static Configuration fromJson(String json) {
        try {
            return new Gson().fromJson(json, Configuration.class);
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
        return null;
    }

    /**
     * This function returns the name of the configuration as defined in the
     * constructor or later using the <code>setName</code> function.
     *
     * @return The name of the configuration
     */
    public String getName() {
        return name;
    }

    /**
     * Use this function to change the name of the configuration.
     *
     * @param name - The name as String to be used for the configuration
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Use this function to add a component to the configuration. A configuration
     * consists of several components which contains the relevant information for
     * each configuration segment.
     *
     * @param component - The component to be added to the configuration
     */
    public void addComponent(Component component) {
        this.components.add(component);
    }

    /**
     * This function returns the number of components attached to this
     * configuration. Use this to iterate over the configurations using
     * <code>getComponentAt</code>.
     *
     * @return Number of components attached to this configuration
     * @see #getComponentAt(int)
     */
    public int getComponentCount() {
        return components.size();
    }

    /**
     * This function returns the <code>Component</code> object listed at the given
     * index in the configuration. Use {@link #getComponentCount()} to determine the
     * number of available components.
     *
     * @param index - The index of the component to return
     * @return The component at the given index
     * @see #getComponentCount()
     */
    public Component getComponentAt(int index) {
        return components.get(index);
    }

    /**
     * Removes the element at the specified position in this list. Shifts any
     * subsequent elements to the left (subtracts one from their indices). Returns
     * the element that was removed from the list.
     *
     * @param index The index of the component to be removed
     * @return The component, which has been removed
     */
    public Component removeComponent(int index) {
        return components.remove(index);
    }

    /**
     * Use this function to add a generator to the configuration. A configuration
     * may consists of several generators which contains the relevant information
     * for a specific generation process.
     *
     * @param generator - The generator description to be added to the configuration
     */
    public void addGenerator(GeneratorDescription generator) {
        generators.add(generator);
    }

    /**
     * Use this function to add a generator to the configuration. A configuration
     * may consists of several generators which contains the relevant information
     * for a specific generation process.
     *
     * @param generator - The generator to be added to the configuration
     */
    public void addGenerator(Generator generator) {
        generators.add(GeneratorFactory.getDescription(generator));
    }

    /**
     * This function returns the number of generators attached to this
     * configuration. Use this to iterate over the configurations using
     * <code>getGeneratorAt</code>.
     *
     * @return Number of generators attached to this configuration
     * @see #getGeneratorAt(int)
     */
    public int getGeneratorCount() {
        return generators.size();
    }

    /**
     * This function returns the <code>Generator</code> object listed at the given
     * index in the configuration. Use {@link #getGeneratorCount()} to determine the
     * number of available components.
     *
     * @param index - The index of the generator to return
     * @return The generator at the given index
     * @see #getGeneratorCount()
     */
    public GeneratorDescription getGeneratorAt(int index) {
        return generators.get(index);
    }

    /**
     * Removes the element at the specified position in this list. Shifts any
     * subsequent elements to the left (subtracts one from their indices). Returns
     * the element that was removed from the list.
     *
     * @param index The index of the generator to be removed
     * @return The generator, which has been removed
     */
    public GeneratorDescription removeGenerator(int index) {
        return generators.remove(index);
    }

    /**
     * This function is collecting recursively all commands from the assigned
     * components and returning a complete list of all activated commands. If there
     * are no active commands an empty list object is returned.
     *
     * @return List of all commands from all active components and variants
     */
    public List<String> getCommands() {
        List<String> commandlist = new ArrayList<>();

        /*
         * Iterating over all known Components and collecting all commands to get a
         * complete command structure for the configuration
         */

        // TODO: Undefined what happens with duplicate commands -> SingletonMap?
        for (Component component : components) {
            commandlist.addAll(component.getCommands());
        }

        return commandlist;
    }

    /**
     * This function returns a complete list of all variable assigned to the
     * components and their activated variants of this configuration.
     *
     * @return A Map object containing all key value pairs of the the variables
     */
    public Map<String, String> getVariables() {
        Map<String, String> variablelist = new HashMap<>();

        /*
         * Iterating over all known Variants and collecting all commands to get a
         * complete command structure for the component
         */
        for (Component component : components) {
            // TODO: Undefined what happens with duplicate variable keys -> SingletonMap?
            variablelist.putAll(component.getVariables());
        }

        return variablelist;
    }

    /**
     * This function has been overridden and returns the complete object structure
     * as JSON string.
     *
     * @return JSON representation of the object structure
     */
    @Override
    public String toString() {
        return Configuration.toJson(this);
    }
}
