package de.alx_development.MazeRunner.UI;

/*-
 * #%L
 * MazeRunner configuration management
 * %%
 * Copyright (C) 2019 ALX-Development
 * %%
 * This file is part of MazeRunner.
 *
 * MazeRunner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MazeRunner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil des MazeRunner Projekt.
 *
 * MazeRunner ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * MazeRunner wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import de.alx_development.MazeRunner.Configuration.Configuration;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;

/**
 * @author Alexander Thiel
 */
public class ConfigurationEditorPanel extends JPanel {

    /**
     *
     */
    private static final long serialVersionUID = -851801769914074794L;

    private static final ImageIcon icon_config = new ImageIcon(Configuration.class.getResource("Configuration.ICON.16x16.png"));
    private static final ImageIcon icon_source = new ImageIcon(Configuration.class.getResource("ConfigurationSource.ICON.16x16.png"));

    /**
     *
     */
    public ConfigurationEditorPanel(Configuration config) {
        super();
        setLayout(new BorderLayout());

        JTabbedPane tabbedPane = new JTabbedPane();

        JLabel panel1 = new JLabel("Panel #1");
        tabbedPane.addTab("Konfiguration", icon_config, panel1, "Does nothing");
        tabbedPane.setMnemonicAt(0, KeyEvent.VK_1);

        ConfigurationValuesDisplay panel2 = new ConfigurationValuesDisplay(config);
        tabbedPane.addTab("Quellcode", icon_source, panel2, "Displays the resulting configuration source");
        tabbedPane.setMnemonicAt(1, KeyEvent.VK_2);

        add(tabbedPane, BorderLayout.CENTER);
    }

}
