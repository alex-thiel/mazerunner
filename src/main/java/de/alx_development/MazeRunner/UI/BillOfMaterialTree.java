package de.alx_development.MazeRunner.UI;

/*-
 * #%L
 * MazeRunner configuration management
 * %%
 * Copyright (C) 2019 ALX-Development
 * %%
 * This file is part of MazeRunner.
 *
 * MazeRunner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MazeRunner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil des MazeRunner Projekt.
 *
 * MazeRunner ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * MazeRunner wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import de.alx_development.MazeRunner.BillOfMaterial.BillOfMaterialElement;
import de.alx_development.MazeRunner.BillOfMaterial.BillOfMaterialTreeModel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * @author Alexander Thiel
 */
public class BillOfMaterialTree extends JComponent {

    /**
     *
     */
    private static final long serialVersionUID = 873653770134855375L;
    JTree bomTree;

    public BillOfMaterialTree() {
        this.setLayout(new BorderLayout());
        bomTree = new JTree();
        bomTree.setModel(null);
        bomTree.setLayout(new BorderLayout());

        // bomTree.setCellRenderer(new BillOfMaterialTreeCellRenderer());
        JScrollPane treeScrollPane = new JScrollPane(bomTree);
        this.add(treeScrollPane, BorderLayout.CENTER);

        bomTree.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                if (e.getButton() == 3) {
                    Object o = e.getSource();
                    JTree nodeInfo = (JTree) o;
                    BillOfMaterialElement bom = (BillOfMaterialElement) nodeInfo.getLastSelectedPathComponent();

                    JDialog dialog = new JDialog();
                    dialog.setBounds(0, 0, 400, 400);
                    JLabel label = new JLabel();
                    label.setBounds(10, 10, 100, 50);
                    label.setText("POS   " + bom.getAttribute("Pos"));
                    dialog.add(label);

                    JLabel label2 = new JLabel();
                    label2.setBounds(10, 70, 100, 50);
                    label2.setText("TYP   " + bom.getAttribute("Typ"));
                    dialog.add(label2);

                    JLabel label3 = new JLabel();
                    label3.setBounds(10, 130, 100, 50);
                    label3.setText("POS Text   " + bom.getAttribute("Positionstext 1"));
                    dialog.add(label3);
                    JLabel label4 = new JLabel();
                    label4.setBounds(10, 190, 100, 50);
                    label4.setText("Standardangaben   " + bom.getAttribute("Standardangaben"));
                    dialog.add(label4);

                    dialog.setVisible(true);
                }
            }
        });
    }

    public void setModel(BillOfMaterialTreeModel model) {
        bomTree.setModel(model);
    }
}
