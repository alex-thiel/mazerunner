package de.alx_development.MazeRunner.UI;

/*-
 * #%L
 * MazeRunner configuration management
 * %%
 * Copyright (C) 2019 ALX-Development
 * %%
 * This file is part of MazeRunner.
 *
 * MazeRunner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MazeRunner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil des MazeRunner Projekt.
 *
 * MazeRunner ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * MazeRunner wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import de.alx_development.MazeRunner.BillOfMaterial.BillOfMaterial;
import de.alx_development.MazeRunner.BillOfMaterial.BillOfMaterialElement;
import de.alx_development.MazeRunner.BillOfMaterial.BillOfMaterialListCellRenderer;
import de.alx_development.MazeRunner.BillOfMaterial.BillOfMaterialListModel;

import javax.swing.*;
import java.awt.*;

/**
 * @author Alexander Thiel
 */
public class BillOfMaterialList extends JComponent {

    private static final long serialVersionUID = -6548208427406226910L;

    JList<BillOfMaterialElement> bomList;

    public BillOfMaterialList() {
        bomList = new JList<>();
        bomList.setCellRenderer(new BillOfMaterialListCellRenderer());
        bomList.setLayout(new BorderLayout());

        JScrollPane listScrollPane = new JScrollPane(bomList, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        this.setLayout(new BorderLayout());
        this.add(listScrollPane, BorderLayout.CENTER);
    }

    public void setModel(BillOfMaterialListModel model) {
        bomList.setModel(model);
    }

    public BillOfMaterial getBillOfMaterial() {
        return ((BillOfMaterialListModel) bomList.getModel()).getBillOfMaterial();
    }

    public void setBillOfMaterial(BillOfMaterial bom) {
        this.setModel(new BillOfMaterialListModel(bom));
    }
}
