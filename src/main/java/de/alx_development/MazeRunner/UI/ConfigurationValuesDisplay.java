package de.alx_development.MazeRunner.UI;

/*-
 * #%L
 * MazeRunner configuration management
 * %%
 * Copyright (C) 2019 ALX-Development
 * %%
 * This file is part of MazeRunner.
 *
 * MazeRunner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MazeRunner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil des MazeRunner Projekt.
 *
 * MazeRunner ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * MazeRunner wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import de.alx_development.MazeRunner.Configuration.Configuration;

import javax.swing.*;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import java.awt.*;
import java.util.Iterator;
import java.util.Map;

/**
 * @author Alexander Thiel
 */
public class ConfigurationValuesDisplay extends JPanel {

    /**
     *
     */
    private static final long serialVersionUID = -3214334662340946270L;
    private final JTextPane jTextPane = new JTextPane();

    public ConfigurationValuesDisplay(Configuration config) {
        super();
        JScrollPane jScrollPane = new JScrollPane();

        // Setting up the stylesheet
        HTMLEditorKit editorkit = new HTMLEditorKit();
        StyleSheet defaultStyle = editorkit.getStyleSheet();
        StyleSheet style = new StyleSheet();
        style.addStyleSheet(defaultStyle);
        style.addStyleSheet(StyleDefinition.getStyleSheet());
        editorkit.setStyleSheet(style);

        // Setting up the JTextPane
        jTextPane.setEditable(false);
        jTextPane.setOpaque(true);
        jTextPane.setEditorKit(editorkit);

        // Setting up the enclosing scroll pane
        jScrollPane.setBounds(new Rectangle(40, 30, 150, 50));
        jScrollPane.getViewport().add(jTextPane, null);

        // Adding all to this panel
        setLayout(new BorderLayout());
        add(jScrollPane, BorderLayout.CENTER);

        // Running the update procedure to build the
        // content which should be displayed
        update(config);
    }


    private void update(Configuration config) {
        /*
         * The StringBuilder is ideal for building a dynamic html
         * string while traversing into the configuration values
         */
        StringBuilder htmlcode = new StringBuilder("<HTML><BODY>");

        /*
         * Building the commands view
         */
        htmlcode.append(String.format("<h1>%s</h1>", "Configurator Commands"));

        htmlcode.append("<table>");
        htmlcode.append(String.format("<tr><th>%s</th></tr>", "Commands"));
        Iterator<String> itr = config.getCommands().iterator();
        while (itr.hasNext()) {
            htmlcode.append(String.format("<tr><td>%s</td></tr>", itr.next()));
        }
        htmlcode.append("</table>");

        /*
         * Building the variables view
         */
        htmlcode.append(String.format("<h1>%s</h1>", "Configurator Variables"));

        htmlcode.append("<table>");
        htmlcode.append(String.format("<tr><th>%s</th><th>%s</th></tr>", "Variable Key", "Value"));
        Map<String, String> variables = config.getVariables();
        itr = variables.keySet().iterator();
        while (itr.hasNext()) {
            String key = itr.next();
            htmlcode.append(String.format("<tr><td>%s</td><td>%s</td></tr>", key, variables.get(key)));
        }
        htmlcode.append("</table>");

        /*
         * Finishing the HTML content and setting the content into
         * the JTextPane.
         */
        htmlcode.append("</BODY></HTML>");
        jTextPane.setText(htmlcode.toString());
    }
}
