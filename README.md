![Bitbucket Pipelines](https://img.shields.io/bitbucket/pipelines/alex-thiel/mazerunner/master)
![Bitbucket open issues](https://img.shields.io/bitbucket/issues-raw/alex-thiel/mazerunner)

# MazeRunner - der Weg durch das Labyrinth #

Es ist nichts Neues, dass das Engineering der Schlüssel für Unternehmenserfolg im Maschinen- und Anlagenbau ist. Denn hier liegt das gesamte Know-how des Unternehmens. Umso wichtiger ist, dass das Engineering von zeitraubenden Routinetätigkeiten befreit wird.

Auch belegen aktuelle Studien von VDI, VDMA, dass in den Entwicklungsbereichen des Maschinen- und Anlagenbaus die Routinetätigkeiten über 50 % liegen.

Es liegt daher auf der Hand, dass Automatisierung, Konfiguration und Generierung die Schlüssel für zukünftiges Produktengineering sind. Insbesondere im Bereich der Konfiguration gibt es viele Systeme bereits am Markt. Warum daher ein neuer Konfigurator?

## Konfigurator oder Konfigurationsmanager? ##

MazeRunner ist kein klassischer Konfigurator, wie diese für Vertriebszwecke oder ähnliches verwendet werden. Es geht bei diesem Projekt nicht darum Laien mit einfachen, grafischen und regelbasierten Elementen zu einer korrekten Produktbeschreibung zu führen. Vielmehr der technische Ansatz und die Wegbegleitung durch das Labyrinth aus Konfiguration, Generatoren und Einstellungen soll der MazeRunner erleichtern.

Hierzu können bereits durch einen Vertriebskonfigurator erstellte Produktbeschreibungen als Eingabeinformation direkt in MazeRunner eingelesen werden. Die hierin enthaltenen Informationen können direkt über ein frei definierbares Regelwerk in eine technische Beschreibung überführt werden. Im Zuge dieser Transformation stehen neben reinem Beziehungsabbildungen auch Berechnungen zu Verfügung.

Der resultierende technische Datensatz wiederum wird durch entsprechende Generator-Beschreibungen in eine für diverse Generatoren geeignete Form gewandelt und diesen dann zugeführt. Generatoren werden dabei vom MazeRunner überwacht und deren Ergebnisse ggf. der Basiskonfiguration wieder zugeführt. Hierdurch können auch aufeinander aufbauende Systeme direkt im MazeRunner miteinander verbunden werden.

Aus der Softwareentwicklungen sind ähnliche Systeme als Build-Tools bekannt. Um die Transformation von Code in ein ausführbares Anwendungsprogramm zu erleichtern, gibt es diese Build-Tools, die den Erstellungsprozess übernehmen und neben der Code-Kompilierung u.a. die Einbindung der nötigen Bibliotheken erledigen. Bekannte Vertreter dieser Tools sind make, ant oder Maven (es gibt viele weitere, diese seien nur als Beispiele erwähnt).

Der Erstellungsprozess mit Build-Tools besteht typischerweise aus der Kompilierung und dem Linken des kompilierten Codes an Bibliotheken. Da der Erstellungsprozess automatisiert ausgeführt wird, benötigt das ausführende Erstellungsprogramm eine formale Beschreibung der durchzuführenden Programm- oder Funktionsaufrufe (Compiler, Linker etc.) sowie der Abhängigkeiten dieser Aufrufe untereinander.

Die Abhängigkeitsbeschreibung orientiert sich beim MazeRunner nicht, wie bei vielen Build-Tools aus der Softwareentwicklung auf Basis der verwendeten Dateiendungen, die transformiert werden sollen, sondern auf Rule-Sets, die aus der importierten Produktbeschreibung ein virtuelles Abbild schaffen.

# Lizenz #

MazeRunner ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren.

MazeRunner wird in der Hoffnung, dass es nützlich sein wird, aber OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK. Siehe die GNU General Public License für weitere Details.

Mit dem Programm erhalten sie eine Kopie der GNU General Public License. Wenn nicht, siehe <https://www.gnu.org/licenses/> für die komplette Lizenzdatei.

Copyright (c) 2019 ALX-Development

<http://https://www.alx-development.de/>
